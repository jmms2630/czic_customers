package com.bbva.czic.customers;

import com.bbva.ccol.commons.rm.utils.converters.DateConverter;
import com.bbva.ccol.commons.rm.utils.converters.DateFormatEnum;
import com.bbva.czic.customers.business.dto.*;
import com.bbva.czic.customers.facade.v02.dto.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Entelgy on 24/02/2016.
 */
public class DummyMock {

	public DTOIntCustomerV2 getDTOIntCustomer() {

		final DTOIntCustomerV2 dtoIntCustomer = new DTOIntCustomerV2();
		DTOIntType dtoIntType = new DTOIntType();
		DTOIntCountry dtoIntCountry = new DTOIntCountry();


		dtoIntCustomer.setCustomerId("12345");
		dtoIntCustomer.setFirstName("jmms");
		dtoIntCustomer.setLastName("sas");
		dtoIntCustomer.setBirthDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
		dtoIntCustomer.setMaritalStatus("Soltero");
		dtoIntCustomer.setGender("Masculino");

		final DTOIntPartnerInformation dtoIntPartnerInformation = new DTOIntPartnerInformation();
			dtoIntType.setId("01");
			dtoIntType.setName("CC");
			dtoIntPartnerInformation.setName("Ana");
			dtoIntPartnerInformation.setDocumentNumber("1234567890");
			dtoIntPartnerInformation.setDocumentType(dtoIntType);
		dtoIntCustomer.setPartnerInformation(dtoIntPartnerInformation);

		final List<DTOIntNationalities> dtoIntNationalities = new ArrayList<DTOIntNationalities>();
		DTOIntNationalities dtoIntNationality = new DTOIntNationalities();
		dtoIntNationality.setId("57");
		dtoIntNationality.setName("Colombia");
		dtoIntNationalities.add(dtoIntNationality);
		dtoIntNationality = new DTOIntNationalities();
		dtoIntNationality.setId("58");
		dtoIntNationality.setName("EEUU");
		dtoIntNationalities.add(dtoIntNationality);
		dtoIntNationality = new DTOIntNationalities();
		dtoIntNationality.setId("70");
		dtoIntNationality.setName("España");
		dtoIntNationalities.add(dtoIntNationality);
		dtoIntNationality = new DTOIntNationalities();
		dtoIntNationality.setId("30");
		dtoIntNationality.setName("Ecuador");
		dtoIntNationalities.add(dtoIntNationality);
		dtoIntCustomer.setNationalities(dtoIntNationalities);

		dtoIntCustomer.setHasPassesAway("no");

		final DTOIntIdentityDocuments dtoIntIdentityDocument = new DTOIntIdentityDocuments();
			dtoIntType = new DTOIntType();
			dtoIntType.setId("01");
			dtoIntType.setName("CC");
			dtoIntIdentityDocument.setDocumentType(dtoIntType);
			dtoIntIdentityDocument.setDocumentNumber("102398765");
			dtoIntIdentityDocument.setExpeditionDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
			dtoIntIdentityDocument.setExpirationDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
			dtoIntCountry.setId("57");
			dtoIntCountry.setName("Colombia");
			dtoIntIdentityDocument.setCountryExpedition(dtoIntCountry);
			dtoIntIdentityDocument.setStateExpedition("Cundinamarca");
			dtoIntIdentityDocument.setCityExpedition("Bogota");

		dtoIntCustomer.setIdentityDocument(dtoIntIdentityDocument);


		dtoIntCustomer.setNumberPersonAssociated("3");

		final DTOIntLegalAddresses dtoIntLegalAddress = new DTOIntLegalAddresses();
			dtoIntCountry = new DTOIntCountry();
			dtoIntCountry.setId("67");
			dtoIntCountry.setName("Peru");
			dtoIntLegalAddress.setCountry(dtoIntCountry);
			dtoIntLegalAddress.setType("2");
			dtoIntLegalAddress.setRegion("pe");
			dtoIntLegalAddress.setCity("lima");
			dtoIntLegalAddress.setAddressName("Dir 001");
			dtoIntLegalAddress.setZipCode("1001100");
		dtoIntCustomer.setLegalAddresses(dtoIntLegalAddress);

		final List<DTOIntContactDetail> dtoIntContactDetails = new ArrayList<DTOIntContactDetail>();
		DTOIntContactDetail dtoIntContactDetail = new DTOIntContactDetail();
			dtoIntType = new DTOIntType();
			dtoIntType.setId("01");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("1122334455");
			dtoIntContactDetail.setContact("qw");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("02");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("2222334455");
			dtoIntContactDetail.setContact("qe");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("03");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("3322334455");
			dtoIntContactDetail.setContact("qr");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("04");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("4422334455");
			dtoIntContactDetail.setContact("qt");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("05");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("5522334455");
			dtoIntContactDetail.setContact("qy");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("06");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("6622334455");
			dtoIntContactDetail.setContact("qu");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("07");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("7722334455");
			dtoIntContactDetail.setContact("qi");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("08");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("8822334455");
			dtoIntContactDetail.setContact("qo");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("09");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("9922334455");
			dtoIntContactDetail.setContact("qp");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);
			dtoIntContactDetail = new DTOIntContactDetail();

			dtoIntType = new DTOIntType();
			dtoIntType.setId("10");
			dtoIntType.setName("CE");
			dtoIntContactDetail.setContactType(dtoIntType);
			dtoIntContactDetail.setId("1022334455");
			dtoIntContactDetail.setContact("qs");
			dtoIntContactDetail.setIsChecked(Boolean.FALSE);
			dtoIntContactDetail.setIsPreferential(Boolean.TRUE);
			dtoIntContactDetails.add(dtoIntContactDetail);

			dtoIntCustomer.setContactDetail(dtoIntContactDetails);

		final DTOIntBankingRelationship dtoIntBankingRelationship =new DTOIntBankingRelationship();
		dtoIntBankingRelationship.setCustomerFlag("cfl");
		dtoIntBankingRelationship.setDebitFlag("dfl");
		dtoIntBankingRelationship.setPremiumFlag("pfl");
		dtoIntBankingRelationship.setManager("Mng");
		dtoIntCustomer.setBankingRelationship(dtoIntBankingRelationship);

		final DTOIntLevelStudy dtoIntLevelStudy = new DTOIntLevelStudy();
		dtoIntCountry = new DTOIntCountry();
		dtoIntCountry.setId("49");
		dtoIntCountry.setName("Argentina");
		dtoIntLevelStudy.setCountry(dtoIntCountry);
		dtoIntLevelStudy.setId("03");
		dtoIntLevelStudy.setTitle("Ing ind");
		dtoIntLevelStudy.setState("DC");
		dtoIntLevelStudy.setCity("Buenos Aires");
		dtoIntCustomer.setLevelStudy(dtoIntLevelStudy);


		final DTOIntLivingPlace dtoIntLivingPlace = new DTOIntLivingPlace();
		dtoIntType = new DTOIntType();
		dtoIntType.setId("001");
		dtoIntType.setName("casa");
		dtoIntLivingPlace.setLivingtype(dtoIntType);
		dtoIntType = new DTOIntType();
		dtoIntType.setId("0002");
		dtoIntType.setName("poi");
		dtoIntLivingPlace.setRelationType(dtoIntType);
		dtoIntLivingPlace.setLivingLevel(3);
		dtoIntCustomer.setLivingPlace(dtoIntLivingPlace);

		final DTOIntEmploymentInformation dtoIntEmploymentInformation = new DTOIntEmploymentInformation();
		dtoIntEmploymentInformation.setJobTittle("consultor");
		dtoIntEmploymentInformation.setIsHolder("no");
		dtoIntEmploymentInformation.setTypeEmployee("consultor j");
		dtoIntEmploymentInformation.setStartDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
		dtoIntEmploymentInformation.setYearsAntiguaty(2);
		final DTOIntPurpouseCompany dtoIntPurpouseCompany = new DTOIntPurpouseCompany();
		dtoIntPurpouseCompany.setId("45545");
		dtoIntPurpouseCompany.setName("BBVA");
		dtoIntPurpouseCompany.setCode("1111111111-0");
		dtoIntEmploymentInformation.setPurpouseCompany(dtoIntPurpouseCompany);
		dtoIntCustomer.setEmploymentInformation(dtoIntEmploymentInformation);

		dtoIntCustomer.setIsPublicPerson("si");


		return dtoIntCustomer;
	}
	
	public DataCustomer CreateMapInCustomer(){
		final Customer customer = new Customer();
		Type type = new Type();
		Country country = new Country();

		customer.setCustomerId("12345");
		customer.setFirstName("jmms");
		customer.setLastName("sas");
		customer.setBirthDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
		customer.setMaritalStatus("Soltero");
		customer.setGender("Masculino");
		
			final List<PartnerInformation> partnerInformations = new ArrayList<PartnerInformation>();
			final PartnerInformation partnerInformation = new PartnerInformation();
			type.setId("01");
			type.setName("CC");
			partnerInformation.setName("Ana");
			partnerInformation.setDocumentNumber("1234567890");
			partnerInformation.setDocumentType(type);
			partnerInformations.add(partnerInformation);
			customer.setPartnerInformation(partnerInformations);

			final List<Nationalities> nationalities = new ArrayList<Nationalities>();
			Nationalities Nationality = new Nationalities();
			Nationality.setId("57");
			Nationality.setName("Colombia");
			nationalities.add(Nationality);
			Nationality = new Nationalities();
			Nationality.setId("58");
			Nationality.setName("EEUU");
			nationalities.add(Nationality);
			Nationality = new Nationalities();
			Nationality.setId("70");
			Nationality.setName("España");
			nationalities.add(Nationality);
			Nationality = new Nationalities();
			Nationality.setId("30");
			Nationality.setName("Ecuador");
			nationalities.add(Nationality);
			customer.setNationalities(nationalities);

			customer.setHasPassesAway("no");
			
			final  List<IdentityDocuments> identityDocuments = new ArrayList<IdentityDocuments>();
			final IdentityDocuments identityDocument = new IdentityDocuments();
			type = new Type();
			type.setId("01");
			type.setName("CC");
			identityDocument.setDocumentType(type);
			identityDocument.setDocumentNumber("102398765");
			identityDocument.setExpeditionDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
			identityDocument.setExpirationDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
			country.setId("57");
			country.setName("Colombia");
			identityDocument.setCountryExpedition(country);
			identityDocument.setStateExpedition("Cundinamarca");
			identityDocument.setCityExpedition("Bogota");
			identityDocuments.add(identityDocument);
			customer.setIdentityDocuments(identityDocuments);


			customer.setNumberPersonAssociated("3");
			
			final List<LegalAddresses> legalAddresses = new ArrayList<LegalAddresses>();
			final LegalAddresses legalAddress = new LegalAddresses();
			country = new Country();
			country.setId("67");
			country.setName("Peru");
			legalAddress.setCountry(country);
			legalAddress.setType("2");
			legalAddress.setRegion("pe");
			legalAddress.setCity("lima");
			legalAddress.setAddressName("Dir 001");
			legalAddress.setZipCode("1001100");
			legalAddresses.add(legalAddress);
			customer.setLegalAddresses(legalAddresses);

			final List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
			ContactDetail contactDetail = new ContactDetail();
			type = new Type();
			type.setId("01");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("1");
			contactDetail.setContact("qw");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);
		
			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("02");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("2");
			contactDetail.setContact("qe");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);
		
			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("03");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("3");
			contactDetail.setContact("qr");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);
		
			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("04");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("4");
			contactDetail.setContact("qt");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);
		
			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("05");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("5");
			contactDetail.setContact("qy");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);
		
			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("06");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("6");
			contactDetail.setContact("qu");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);

			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("07");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("7");
			contactDetail.setContact("qi");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);

			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("08");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("8");
			contactDetail.setContact("qo");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);

			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("09");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("9");
			contactDetail.setContact("qp");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);

			contactDetail = new ContactDetail();
			type = new Type();
			type.setId("10");
			type.setName("CE");
			contactDetail.setContactType(type);
			contactDetail.setId("10");
			contactDetail.setContact("qs");
			contactDetail.setIsChecked(Boolean.FALSE);
			contactDetail.setIsPreferential(Boolean.TRUE);
			contactDetails.add(contactDetail);

		customer.setContactDetails(contactDetails);

		final BankingRelationship bankingRelationship =new BankingRelationship();
			bankingRelationship.setCustomerFlag("cfl");
			bankingRelationship.setDebitFlag("dfl");
			bankingRelationship.setPremiumFlag("pfl");
			bankingRelationship.setManager("Mng");
			customer.setBankingRelationship(bankingRelationship);

			final LevelStudy levelStudy = new LevelStudy();
			country = new Country();
			country.setId("49");
			country.setName("Argentina");
			levelStudy.setCountry(country);
			levelStudy.setId("03");
			levelStudy.setTitle("Ing ind");
			levelStudy.setState("DC");
			levelStudy.setCity("Buenos Aires");
			customer.setLevelStudy(levelStudy);


			final LivingPlace livingPlace = new LivingPlace();
			type = new Type();
			type.setId("001");
			type.setName("casa");
			livingPlace.setLivingtype(type);
			type = new Type();
			type.setId("0002");
			type.setName("poi");
			livingPlace.setRelationType(type);
			livingPlace.setLivingLevel(3);
			customer.setLivingPlace(livingPlace);

			final EmploymentInformation employmentInformation = new EmploymentInformation();
			employmentInformation.setJobTittle("consultor");
			employmentInformation.setIsHolder("no");
			employmentInformation.setTypeEmployee("consultor j");
			employmentInformation.setStartDate(new DateConverter().toDate("2016-08-24", DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
			employmentInformation.setYearsAntiguaty(2);
			final PurpouseCompany purpouseCompany = new PurpouseCompany();
			purpouseCompany.setId("45545");
			purpouseCompany.setName("BBVA");
			purpouseCompany.setCode("1111111111-0");
			employmentInformation.setPurpouseCompany(purpouseCompany);
			customer.setEmploymentInformation(employmentInformation);

			customer.setIsPublicPerson("si");
			final DataCustomer dataCustomer = new DataCustomer();
			dataCustomer.setData(customer);
			return dataCustomer;
	}
}
