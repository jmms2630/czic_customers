package com.bbva.czic.customers.business.dto;

import com.bbva.zic.utilTest.TestUtils;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.filters.FilterChain;
import com.openpojo.reflection.filters.FilterEnum;
import com.openpojo.reflection.filters.FilterPackageInfo;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.affirm.Affirm;
import org.junit.Test;

import java.util.List;

/**
 * Created by Entelgy on 30/11/2015.
 */
public class DTOInternalTest {

	// Configured for expectation, so we know when a class gets added or removed.
	private static final int EXPECTED_CLASS_COUNT = 15;

	// The packages to test
	private static final String POJO_PACKAGE = "com.bbva.czic.customers.business.dto";

	// OpenPojo validator
	protected Validator validator;

	private List<PojoClass> pojoClasses;

	@Test
	public void testPojos() {
		this.validator = TestUtils.setUpPojoValidator();
		this.pojoClasses = PojoClassFactory.getPojoClasses(POJO_PACKAGE, new FilterChain(new FilterPackageInfo(),
				new FilterEnum()));
		Affirm.affirmEquals("Classes added / removed?", EXPECTED_CLASS_COUNT, this.pojoClasses.size());
		this.validator.validate(POJO_PACKAGE, new FilterPackageInfo());
		TestUtils.testToString(this.pojoClasses);
	}

}
