package com.bbva.czic.customers.business.impl;


import com.bbva.czic.customers.DummyMock;
import com.bbva.czic.customers.business.ISrvIntCustomer;
import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.business.impl.SrvIntCustomer;
import com.bbva.czic.customers.dao.ICustomerDAO;
import com.bbva.czic.customers.dao.impl.CustomerDao;
import com.bbva.czic.customers.facade.v02.dto.DataCustomer;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;


public class SrvIntCustomersTest {

	@Mock
	private ICustomerDAO customerDAO;

	@InjectMocks
	private ISrvIntCustomer iSrvIntCustomer;

	private DummyMock dummymock;

	@Before
	public void init() {
		iSrvIntCustomer = new SrvIntCustomer();
		dummymock = new DummyMock();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetCustomer() {

		final DTOIntCustomerV2 dtoIntCustomerV2 = dummymock.getDTOIntCustomer();

		Mockito.when(customerDAO.getCustomer("123")).thenReturn(dtoIntCustomerV2);

		final DTOIntCustomerV2 customer = iSrvIntCustomer.getCustomer("123");

		Assert.assertNotNull(customer);
		Assert.assertEquals(customer.getCustomerId(), dtoIntCustomerV2.getCustomerId());
		Assert.assertEquals(customer.getFirstName(), dtoIntCustomerV2.getFirstName());
		Assert.assertEquals(customer.getLastName(), dtoIntCustomerV2.getLastName());
		Assert.assertEquals(customer.getBirthDate(),dtoIntCustomerV2.getBirthDate());
		Assert.assertEquals(customer.getMaritalStatus(),dtoIntCustomerV2.getMaritalStatus());
		Assert.assertEquals(customer.getGender(), dtoIntCustomerV2.getGender());

		Assert.assertEquals(customer.getPartnerInformation().getDocumentType().getId(), dtoIntCustomerV2
				.getPartnerInformation().getDocumentType().getId());
		Assert.assertEquals(customer.getPartnerInformation().getDocumentType().getName(), dtoIntCustomerV2
				.getPartnerInformation().getDocumentType().getName());
		Assert.assertEquals(customer.getPartnerInformation().getDocumentNumber(), dtoIntCustomerV2
				.getPartnerInformation().getDocumentNumber());
		Assert.assertEquals(customer.getPartnerInformation().getName(), dtoIntCustomerV2
				.getPartnerInformation().getName());
		for (int i=0 ; i<=3 ;i++) {
			Assert.assertEquals(customer.getNationalities().get(i).getId(), dtoIntCustomerV2.getNationalities().get(i).getId());
			Assert.assertEquals(customer.getNationalities().get(i).getName(), dtoIntCustomerV2.getNationalities().get(i).getName());
		}
		Assert.assertEquals(customer.getHasPassesAway(),dtoIntCustomerV2.getHasPassesAway());

		Assert.assertEquals(customer.getIdentityDocument().getDocumentType().getId(),dtoIntCustomerV2
				.getIdentityDocument().getDocumentType().getId());
		Assert.assertEquals(customer.getIdentityDocument().getDocumentType().getName(),dtoIntCustomerV2
				.getIdentityDocument().getDocumentType().getName());
		Assert.assertEquals(customer.getIdentityDocument().getDocumentNumber(), dtoIntCustomerV2
				.getIdentityDocument().getDocumentNumber());
		Assert.assertEquals(customer.getIdentityDocument().getExpeditionDate(),dtoIntCustomerV2
				.getIdentityDocument().getExpeditionDate());
		Assert.assertEquals(customer.getIdentityDocument().getExpirationDate(),dtoIntCustomerV2
				.getIdentityDocument().getExpirationDate());
		Assert.assertEquals(customer.getIdentityDocument().getCountryExpedition().getId(),dtoIntCustomerV2
				.getIdentityDocument().getCountryExpedition().getId());
		Assert.assertEquals(customer.getIdentityDocument().getCountryExpedition().getName(),dtoIntCustomerV2
				.getIdentityDocument().getCountryExpedition().getName());
		Assert.assertEquals(customer.getIdentityDocument().getStateExpedition(),dtoIntCustomerV2
				.getIdentityDocument().getStateExpedition());
		Assert.assertEquals(customer.getIdentityDocument().getCityExpedition(),dtoIntCustomerV2
				.getIdentityDocument().getCityExpedition());

		Assert.assertEquals(customer.getNumberPersonAssociated(),dtoIntCustomerV2.getNumberPersonAssociated());

		Assert.assertEquals(customer.getLegalAddresses().getType(),dtoIntCustomerV2
				.getLegalAddresses().getType());
		Assert.assertEquals(customer.getLegalAddresses().getAddressName(),dtoIntCustomerV2
				.getLegalAddresses().getAddressName());
		Assert.assertEquals(customer.getLegalAddresses().getCountry().getId(),dtoIntCustomerV2
				.getLegalAddresses().getCountry().getId());
		Assert.assertEquals(customer.getLegalAddresses().getCountry().getName(),dtoIntCustomerV2
				.getLegalAddresses().getCountry().getName());
		Assert.assertEquals(customer.getLegalAddresses().getRegion(),dtoIntCustomerV2
				.getLegalAddresses().getRegion());
		Assert.assertEquals(customer.getLegalAddresses().getCity(),dtoIntCustomerV2
				.getLegalAddresses().getCity());
		Assert.assertEquals(customer.getLegalAddresses().getZipCode(),dtoIntCustomerV2
				.getLegalAddresses().getZipCode());

		for (int i=0 ; i<=9 ;i++) {
			Assert.assertEquals(customer.getContactDetail().get(i).getId(), dtoIntCustomerV2
					.getContactDetail().get(i).getId());
			Assert.assertEquals(customer.getContactDetail().get(i).getContact(), dtoIntCustomerV2
					.getContactDetail().get(i).getContact());
			Assert.assertEquals(customer.getContactDetail().get(i).getContactType().getId(), dtoIntCustomerV2
					.getContactDetail().get(i).getContactType().getId());
			Assert.assertEquals(customer.getContactDetail().get(i).getContactType().getName(), dtoIntCustomerV2
					.getContactDetail().get(i).getContactType().getName());
			Assert.assertEquals(customer.getContactDetail().get(i).getIsChecked(), dtoIntCustomerV2
					.getContactDetail().get(i).getIsChecked());
			Assert.assertEquals(customer.getContactDetail().get(i).getIsPreferential(), dtoIntCustomerV2
					.getContactDetail().get(i).getIsPreferential());
		}
		Assert.assertEquals(customer.getBankingRelationship().getCustomerFlag(),dtoIntCustomerV2
				.getBankingRelationship().getCustomerFlag());
		Assert.assertEquals(customer.getBankingRelationship().getDebitFlag(),dtoIntCustomerV2
				.getBankingRelationship().getDebitFlag());
		Assert.assertEquals(customer.getBankingRelationship().getPremiumFlag(),dtoIntCustomerV2
				.getBankingRelationship().getPremiumFlag());
		Assert.assertEquals(customer.getBankingRelationship().getManager(),dtoIntCustomerV2
				.getBankingRelationship().getManager());

		Assert.assertEquals(customer.getLevelStudy().getId(),dtoIntCustomerV2.getLevelStudy().getId());
		Assert.assertEquals(customer.getLevelStudy().getTitle(),dtoIntCustomerV2.getLevelStudy().getTitle());
		Assert.assertEquals(customer.getLevelStudy().getCountry().getId(),dtoIntCustomerV2.getLevelStudy().getCountry().getId());
		Assert.assertEquals(customer.getLevelStudy().getCountry().getName(),dtoIntCustomerV2.getLevelStudy().getCountry().getName());
		Assert.assertEquals(customer.getLevelStudy().getState(),dtoIntCustomerV2.getLevelStudy().getState());
		Assert.assertEquals(customer.getLevelStudy().getCity(),dtoIntCustomerV2.getLevelStudy().getCity());

		Assert.assertEquals(customer.getLivingPlace().getLivingtype().getId(),dtoIntCustomerV2.getLivingPlace().getLivingtype().getId());
		Assert.assertEquals(customer.getLivingPlace().getLivingtype().getName(),dtoIntCustomerV2.getLivingPlace().getLivingtype().getName());
		Assert.assertEquals(customer.getLivingPlace().getRelationType().getId(),dtoIntCustomerV2.getLivingPlace().getRelationType().getId());
		Assert.assertEquals(customer.getLivingPlace().getRelationType().getName(),dtoIntCustomerV2.getLivingPlace().getRelationType().getName());
		Assert.assertEquals(customer.getLivingPlace().getLivingLevel(),dtoIntCustomerV2.getLivingPlace().getLivingLevel());

		Assert.assertEquals(customer.getEmploymentInformation().getTypeEmployee(),dtoIntCustomerV2.getEmploymentInformation().getTypeEmployee());
		Assert.assertEquals(customer.getEmploymentInformation().getIsHolder(),dtoIntCustomerV2.getEmploymentInformation().getIsHolder());
		Assert.assertEquals(customer.getEmploymentInformation().getJobTittle(),dtoIntCustomerV2.getEmploymentInformation().getJobTittle());
		Assert.assertEquals(customer.getEmploymentInformation().getStartDate(),dtoIntCustomerV2.getEmploymentInformation().getStartDate());
		Assert.assertEquals(customer.getEmploymentInformation().getYearsAntiguaty(),dtoIntCustomerV2.getEmploymentInformation().getYearsAntiguaty());
		Assert.assertEquals(customer.getEmploymentInformation().getPurpouseCompany().getId(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getId());
		Assert.assertEquals(customer.getEmploymentInformation().getPurpouseCompany().getName(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getName());
		Assert.assertEquals(customer.getEmploymentInformation().getPurpouseCompany().getCode(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getCode());

		Assert.assertEquals(customer.getIsPublicPerson(),dtoIntCustomerV2.getIsPublicPerson());
	}

	@Test
	public void testCreateCustomer(){

		final DTOIntCustomerV2 dtoIntCustomerV2 = dummymock.getDTOIntCustomer();
		Mockito.when(customerDAO.createCustomer(dtoIntCustomerV2)).thenReturn(dtoIntCustomerV2.getCustomerId());
		final String customerNumber = iSrvIntCustomer.createCustomer(dtoIntCustomerV2);

		Assert.assertNotNull(customerNumber);
	}

	@Test
	public void testModifyCustomer(){

		final DTOIntCustomerV2 dtoIntCustomerV2 = dummymock.getDTOIntCustomer();
		iSrvIntCustomer.modifyCustomer(dtoIntCustomerV2);
	}
}