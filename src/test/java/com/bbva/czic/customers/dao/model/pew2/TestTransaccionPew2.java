package com.bbva.czic.customers.dao.model.pew2;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;

/**
 * Test de la transacci&oacute;n <code>PEW2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPew2 {

	@InjectMocks
	private TransaccionPew2 transaccion;

	@Spy
	private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);
	private static final Log LOG = LogFactory.getLog(TestTransaccionPew2.class);

	
	@Test
	public void test() throws ExcepcionTransaccion {
		PeticionTransaccionPew2 peticion = new PeticionTransaccionPew2();
		RespuestaTransaccionPew2 respuesta = new RespuestaTransaccionPew2();

		Mockito.when(
				servicioTransacciones.invocar(PeticionTransaccionPew2.class, RespuestaTransaccionPew2.class, peticion))
				.thenReturn(respuesta);

		RespuestaTransaccionPew2 result = transaccion.invocar(peticion);

		Assert.assertNotNull(result);
	}
}