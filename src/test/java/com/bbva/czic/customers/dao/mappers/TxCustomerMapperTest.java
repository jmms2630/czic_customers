package com.bbva.czic.customers.dao.mappers;

import com.bbva.ccol.commons.rm.utils.converters.DateFormatEnum;
import com.bbva.ccol.commons.rm.utils.converters.DateConverter;
import com.bbva.czic.customers.DummyMock;
import com.bbva.czic.customers.business.dto.*;
import com.bbva.czic.customers.dao.mappers.impl.TxCustomrMapper;
import com.bbva.czic.customers.dao.model.pew2.*;
import com.bbva.czic.customers.dao.model.pew3.FormatoPEW3CE00;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CE00;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class TxCustomerMapperTest {


    private TxCustomrMapper mapper;
    private DummyMock dummyMock;

    @Before
    public void setUp() {
        mapper = new TxCustomrMapper();
        dummyMock = new DummyMock();
    }


    @Test
    public void testMapOutFormatFormatoPEW2CS00GetCustomer() throws Exception {

        final FormatoPEW2CS00 formatOutput = new FormatoPEW2CS00();
        formatOutput.setIdclien("11111");
        formatOutput.setNombre("cliente pr");
        formatOutput.setApellid("pr2");
        formatOutput.setFenacim("1995-12-12");
        formatOutput.setEstaciv("Casado");
        formatOutput.setSexo("Masculino");
        formatOutput.setFalleci("no");
        formatOutput.setNumdepe("2");

        formatOutput.setIugesto("no");
        formatOutput.setPeyclie("12");
        formatOutput.setMarcvip("no");
        formatOutput.setMarcdeb("si");

        formatOutput.setIdnives("03");
        formatOutput.setNiveest("profesional");
        formatOutput.setIdpaini("57");
        formatOutput.setPaisnac("Colombia");
        formatOutput.setDptonac("Cundinamarca");
        formatOutput.setCiudnac("Bogota");

        formatOutput.setIdtipvi("01");
        formatOutput.setNomtipv("casa");
        formatOutput.setIdrelvi("006");
        formatOutput.setNomrelv("ip");
        formatOutput.setEstrato("3");

        formatOutput.setTitulo("Prof");
        formatOutput.setSociemp("no");
        formatOutput.setCargemp("consultor");
        formatOutput.setFeiniem("2016-08-04");
        formatOutput.setAntigue("3");
        formatOutput.setIdnomem("3456");
        formatOutput.setNombemp("BBVA");
        formatOutput.setCiiu("00000000-1");

        formatOutput.setIndpep("no");

        FormatoPEW2CS01 formatOutput1 = new FormatoPEW2CS01();
        formatOutput1.setIdcon01("1");
        formatOutput1.setVlrco01("qw");
        formatOutput1.setIdtic01("0001");
        formatOutput1.setTipco01("f");
        formatOutput1.setEsprf01("n");
        formatOutput1.setEsmrc01("s");

        formatOutput1.setIdcon02("2");
        formatOutput1.setVlrco02("qe");
        formatOutput1.setIdtic02("0002");
        formatOutput1.setTipco02("f");
        formatOutput1.setEsprf02("n");
        formatOutput1.setEsmrc02("s");

        formatOutput1.setIdcon03("3");
        formatOutput1.setVlrco03("qr");
        formatOutput1.setIdtic03("0003");
        formatOutput1.setTipco03("f");
        formatOutput1.setEsprf03("n");
        formatOutput1.setEsmrc03("s");

        formatOutput1.setIdcon04("4");
        formatOutput1.setVlrco04("qt");
        formatOutput1.setIdtic04("0004");
        formatOutput1.setTipco04("f");
        formatOutput1.setEsprf04("n");
        formatOutput1.setEsmrc04("s");

        formatOutput1.setIdcon05("5");
        formatOutput1.setVlrco05("qy");
        formatOutput1.setIdtic05("0005");
        formatOutput1.setTipco05("f");
        formatOutput1.setEsprf05("s");
        formatOutput1.setEsmrc05("n");

        formatOutput1.setIdcon06("6");
        formatOutput1.setVlrco06("qu");
        formatOutput1.setIdtic06("0006");
        formatOutput1.setTipco06("f");
        formatOutput1.setEsprf06("s");
        formatOutput1.setEsmrc06("n");

        formatOutput1.setIdcon07("7");
        formatOutput1.setVlrco07("qi");
        formatOutput1.setIdtic07("0008");
        formatOutput1.setTipco07("f");
        formatOutput1.setEsprf07("n");
        formatOutput1.setEsmrc07("s");

        formatOutput1.setIdcon08("8");
        formatOutput1.setVlrco08("qo");
        formatOutput1.setIdtic08("0008");
        formatOutput1.setTipco08("f");
        formatOutput1.setEsprf08("s");
        formatOutput1.setEsmrc08("n");

        formatOutput1.setIdcon09("9");
        formatOutput1.setVlrco09("qp");
        formatOutput1.setIdtic09("0009");
        formatOutput1.setTipco09("f");
        formatOutput1.setEsprf09("s");
        formatOutput1.setEsmrc09("n");

        formatOutput1.setIdcon10("10");
        formatOutput1.setVlrco10("qa");
        formatOutput1.setIdtic10("0010");
        formatOutput1.setTipco10("f");
        formatOutput1.setEsprf10("s");
        formatOutput1.setEsmrc10("n");

        FormatoPEW2CS02 formatOutput2 = new FormatoPEW2CS02();
        formatOutput2.setTipidcy("01");
        formatOutput2.setNomtidy("CC");
        formatOutput2.setNumidcy("11002299");
        formatOutput2.setNomidcy("luisa");

        FormatoPEW2CS03 formatOutput3 = new FormatoPEW2CS03();
        formatOutput3.setIdnaci1("1");
        formatOutput3.setNacion1("Cuba");

        formatOutput3.setIdnaci2("2");
        formatOutput3.setNacion2("Colombia");

        formatOutput3.setIdnaci3("3");
        formatOutput3.setNacion3("España");

        formatOutput3.setIdnaci4("4");
        formatOutput3.setNacion4("Argentina");

        FormatoPEW2CS04 formatOutput4 = new FormatoPEW2CS04();
        formatOutput4.setIdtipdo("01");
        formatOutput4.setNomtidc("CC");
        formatOutput4.setNumidcl("1234567890");
        formatOutput4.setFeexped("1990-02-01");
        formatOutput4.setFevenci("2050-02-01");
        formatOutput4.setIdpaiex("57");
        formatOutput4.setPaisexp("Colombia");
        formatOutput4.setDptoexp("Cundinamarca");
        formatOutput4.setCiudexp("Bogota");

        FormatoPEW2CS05 formatOutput5 = new FormatoPEW2CS05();
        formatOutput5.setTipdire("01");
        formatOutput5.setIdpaidi("57");
        formatOutput5.setPaisdir("Colombia");
        formatOutput5.setDptodir("Cundinamarca");
        formatOutput5.setCiuddir("Bogota");
        formatOutput5.setDirecci("Cra 20 No. 33A-35");
        formatOutput5.setCodpost("1001100");



        DTOIntCustomerV2 dtoIntCustomer =new DTOIntCustomerV2();
        dtoIntCustomer = mapper.mapOutFormatoPEW2CS00GetCustomer(formatOutput,dtoIntCustomer);

        Assert.assertNotNull(dtoIntCustomer);
        Assert.assertEquals(dtoIntCustomer.getFirstName() ,formatOutput.getNombre());
        Assert.assertEquals(dtoIntCustomer.getLastName(),formatOutput.getApellid());
        Assert.assertEquals(dtoIntCustomer.getBirthDate(), new DateConverter()
                .toDate(formatOutput.getFenacim(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        Assert.assertEquals(dtoIntCustomer.getMaritalStatus(),formatOutput.getEstaciv());
        Assert.assertEquals(dtoIntCustomer.getGender(),formatOutput.getSexo());
        Assert.assertEquals(dtoIntCustomer.getHasPassesAway(),formatOutput.getFalleci());
        Assert.assertEquals(dtoIntCustomer.getNumberPersonAssociated(),formatOutput.getNumdepe());
        Assert.assertEquals(dtoIntCustomer.getIsPublicPerson(),formatOutput.getIndpep());


        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getManager(),formatOutput.getIugesto());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getCustomerFlag(),formatOutput.getPeyclie());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getPremiumFlag(),formatOutput.getMarcvip());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getDebitFlag(),formatOutput.getMarcdeb());

        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getId(),formatOutput.getIdnives());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getTitle(),formatOutput.getNiveest());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCountry().getId(),formatOutput.getIdpaini());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCountry().getName(), formatOutput.getPaisnac());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getState(),formatOutput.getDptonac());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCity(),formatOutput.getCiudnac());

        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingtype().getId(),formatOutput.getIdtipvi());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingtype().getName(), formatOutput.getNomtipv());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getRelationType().getId(), formatOutput.getIdrelvi());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getRelationType().getName(), formatOutput.getNomrelv());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingLevel(),Integer.valueOf(formatOutput.getEstrato()));

        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getJobTittle(), formatOutput.getTitulo());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getIsHolder(),formatOutput.getSociemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getTypeEmployee(),formatOutput.getCargemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getStartDate(),new DateConverter().toDate(formatOutput.getFeiniem(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getYearsAntiguaty(), Integer.valueOf(formatOutput.getAntigue()));
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getId(), formatOutput.getIdnomem());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getName(), formatOutput.getNombemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getCode(),formatOutput.getCiiu());

        dtoIntCustomer = mapper.mapOutFormatoPEW2CS01GetCustomer(formatOutput1,dtoIntCustomer);
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getId(),formatOutput1.getIdcon01());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getContact(),formatOutput1.getVlrco01());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getContactType().getId(), formatOutput1.getIdtic01());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getContactType().getName(),formatOutput1.getTipco01());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getIsPreferential(), StringUtils.equalsIgnoreCase(formatOutput1.getEsprf01(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getIsChecked(), StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc01(), "S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getId(),formatOutput1.getIdcon02());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getContact(),formatOutput1.getVlrco02());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getContactType().getId(), formatOutput1.getIdtic02());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getContactType().getName(),formatOutput1.getTipco02());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf02(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc02(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getId(),formatOutput1.getIdcon03());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getContact(),formatOutput1.getVlrco03());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getContactType().getId(), formatOutput1.getIdtic03());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getContactType().getName(),formatOutput1.getTipco03());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf03(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc03(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getId(),formatOutput1.getIdcon04());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getContact(),formatOutput1.getVlrco04());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getContactType().getId(), formatOutput1.getIdtic04());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getContactType().getName(),formatOutput1.getTipco04());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf04(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc04(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getId(),formatOutput1.getIdcon05());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getContact(),formatOutput1.getVlrco05());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getContactType().getId(), formatOutput1.getIdtic05());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getContactType().getName(),formatOutput1.getTipco05());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf05(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc05(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getId(),formatOutput1.getIdcon06());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getContact(),formatOutput1.getVlrco06());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getContactType().getId(), formatOutput1.getIdtic06());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getContactType().getName(),formatOutput1.getTipco06());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf06(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc06(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getId(),formatOutput1.getIdcon07());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getContact(),formatOutput1.getVlrco07());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getContactType().getId(), formatOutput1.getIdtic07());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getContactType().getName(),formatOutput1.getTipco07());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf07(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc07(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getId(),formatOutput1.getIdcon08());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getContact(),formatOutput1.getVlrco08());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getContactType().getId(), formatOutput1.getIdtic08());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getContactType().getName(),formatOutput1.getTipco08());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf08(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc08(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getId(),formatOutput1.getIdcon09());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getContact(),formatOutput1.getVlrco09());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getContactType().getId(), formatOutput1.getIdtic09());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getContactType().getName(),formatOutput1.getTipco09());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf09(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc09(),"S"));

        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getId(),formatOutput1.getIdcon10());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getContact(),formatOutput1.getVlrco10());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getContactType().getId(), formatOutput1.getIdtic10());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getContactType().getName(),formatOutput1.getTipco10());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getIsPreferential(),StringUtils.equalsIgnoreCase(formatOutput1.getEsprf10(),"S"));
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getIsChecked(),StringUtils.equalsIgnoreCase(formatOutput1.getEsmrc10(),"S"));


        dtoIntCustomer = mapper.mapOutFormatoPEW2CS02GetCustomer(formatOutput2, dtoIntCustomer);
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentType().getId(), formatOutput2.getTipidcy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentType().getName(), formatOutput2.getNomtidy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentNumber(),formatOutput2.getNumidcy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getName(),formatOutput2.getNomidcy());

        dtoIntCustomer = mapper.mapOutFormatoPEW2CS03GetCustomer(formatOutput3,dtoIntCustomer);
        Assert.assertEquals(dtoIntCustomer.getNationalities().get(0).getId(),formatOutput3.getIdnaci1());
        Assert.assertEquals(dtoIntCustomer.getNationalities().get(0).getName(),formatOutput3.getNacion1());

        Assert.assertEquals(dtoIntCustomer.getNationalities().get(1).getId(),formatOutput3.getIdnaci2());
        Assert.assertEquals(dtoIntCustomer.getNationalities().get(1).getName(),formatOutput3.getNacion2());

        Assert.assertEquals(dtoIntCustomer.getNationalities().get(2).getId(),formatOutput3.getIdnaci3());
        Assert.assertEquals(dtoIntCustomer.getNationalities().get(2).getName(),formatOutput3.getNacion3());

        Assert.assertEquals(dtoIntCustomer.getNationalities().get(3).getId(),formatOutput3.getIdnaci4());
        Assert.assertEquals(dtoIntCustomer.getNationalities().get(3).getName(),formatOutput3.getNacion4());



        dtoIntCustomer=mapper.mapOutFormatoPEW2CS04GetCustomer(formatOutput4,dtoIntCustomer);
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentType().getId(),formatOutput4.getIdtipdo());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentType().getName(), formatOutput4.getNomtidc());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentNumber(),formatOutput4.getNumidcl());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getExpeditionDate(), new DateConverter().toDate(formatOutput4.getFeexped(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getExpirationDate(), new DateConverter().toDate(formatOutput4.getFevenci(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getId(), formatOutput4.getIdpaiex());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getName(), formatOutput4.getPaisexp());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getStateExpedition(),formatOutput4.getDptoexp());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCityExpedition(),formatOutput4.getCiudexp());

        dtoIntCustomer = mapper.mapOutFormatoPEW2CS05GetCustomer(formatOutput5,dtoIntCustomer);
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getType(),formatOutput5.getTipdire());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCountry().getId(),formatOutput5.getIdpaidi());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCountry().getName(), formatOutput5.getPaisdir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getRegion(),formatOutput5.getDptodir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCity(),formatOutput5.getCiuddir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getAddressName(),formatOutput5.getDirecci());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getZipCode(),formatOutput5.getCodpost());

    }
    
    @Test
    public void testMapInFormatFormatoPEW4CE00CreateCustomer()throws Exception{
        DTOIntCustomerV2 dtoIntCustomer = dummyMock.getDTOIntCustomer();
        FormatoPEW4CE00 formatInput = mapper.mapInFortmatoPEW4CE00CreateCustomer(dtoIntCustomer);

        Assert.assertNotNull(formatInput);
        Assert.assertEquals(dtoIntCustomer.getFirstName() ,formatInput.getNombre());
        Assert.assertEquals(dtoIntCustomer.getLastName(),formatInput.getApellid());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getBirthDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFenacim());
        Assert.assertEquals(dtoIntCustomer.getMaritalStatus(),formatInput.getEstaciv());
        Assert.assertEquals(dtoIntCustomer.getGender(),formatInput.getSexo());
        Assert.assertEquals(dtoIntCustomer.getHasPassesAway(),formatInput.getFalleci());
        Assert.assertEquals(dtoIntCustomer.getNumberPersonAssociated(),formatInput.getNumdepe());
        Assert.assertEquals(dtoIntCustomer.getIsPublicPerson(),formatInput.getIndpep());


        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getManager(),formatInput.getIugesto());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getCustomerFlag(),formatInput.getPeyclie());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getPremiumFlag(),formatInput.getMarcvip());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getDebitFlag(),formatInput.getMarcdeb());

        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getId(),formatInput.getIdnives());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getTitle(),formatInput.getNiveest());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCountry().getId(),formatInput.getIdpaini());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCountry().getName(), formatInput.getPaisnac());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getState(),formatInput.getDptonac());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCity(),formatInput.getCiudnac());

        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingtype().getId(),formatInput.getIdtipvi());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingtype().getName(), formatInput.getNomtipv());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getRelationType().getId(), formatInput.getIdrelvi());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getRelationType().getName(), formatInput.getNomrelv());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingLevel().toString(),formatInput.getEstrato());

        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getJobTittle(), formatInput.getTitulo());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getIsHolder(),formatInput.getSociemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getTypeEmployee(),formatInput.getCargemp());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getEmploymentInformation().getStartDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFeiniem());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getYearsAntiguaty().toString(), formatInput.getAntigue());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getId(), formatInput.getIdnomem());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getName(), formatInput.getNombemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getCode(),formatInput.getCiiu());

        StringBuilder idConta = new StringBuilder();
        StringBuilder idTipco = new StringBuilder();
        StringBuilder nmTipco = new StringBuilder();
        StringBuilder isprefe = new StringBuilder();
        StringBuilder isCheck = new StringBuilder();
        for (DTOIntContactDetail dtoIntContactDetail:dtoIntCustomer.getContactDetail()) {
            idConta.append(dtoIntContactDetail.getId());
            idTipco.append(dtoIntContactDetail.getContactType().getId());
            nmTipco.append(dtoIntContactDetail.getContactType().getName());
            isprefe.append(dtoIntContactDetail.getIsPreferential().equals(true)?"S":"N");
            isCheck.append(dtoIntContactDetail.getIsChecked().equals(true)?"S":"N");
        }
        Assert.assertEquals(idConta.toString(), formatInput.getIdconta());
        Assert.assertEquals(idTipco.toString(), formatInput.getIdtipco());
        Assert.assertEquals(nmTipco.toString(), formatInput.getNmtipco());
        Assert.assertEquals(isprefe.toString(), formatInput.getIsprefe());
        Assert.assertEquals(isCheck.toString(), formatInput.getIscheck());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getContact(),formatInput.getVlrco01());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getContact(),formatInput.getVlrco02());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getContact(),formatInput.getVlrco03());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getContact(),formatInput.getVlrco04());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getContact(),formatInput.getVlrco05());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getContact(),formatInput.getVlrco06());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getContact(),formatInput.getVlrco07());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getContact(),formatInput.getVlrco08());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getContact(),formatInput.getVlrco09());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getContact(),formatInput.getVlrco10());


        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentType().getId(), formatInput.getTipidcy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentType().getName(), formatInput.getNomtidy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentNumber(),formatInput.getNumidcy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getName(),formatInput.getNomidcy());

        StringBuilder naciona = new StringBuilder();
        naciona.append(dtoIntCustomer.getNationalities().get(0).getId());
        naciona.append(dtoIntCustomer.getNationalities().get(0).getName());
        Assert.assertEquals(naciona.toString(),formatInput.getNaciona());


        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentType().getId(),formatInput.getIdtipdo());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentType().getName(), formatInput.getNomtidc());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentNumber(),formatInput.getNumidcl());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpeditionDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFeexped());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpirationDate(),DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFevenci());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getId(), formatInput.getIdpaiex());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getName(), formatInput.getPaisexp());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getStateExpedition(),formatInput.getDptoexp());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCityExpedition(),formatInput.getCiudexp());

        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getType(),formatInput.getTipdire());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCountry().getId(),formatInput.getIdpaidi());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCountry().getName(), formatInput.getPaisdir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getRegion(),formatInput.getDptodir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCity(),formatInput.getCiuddir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getAddressName(),formatInput.getDirecci());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getZipCode(),formatInput.getCodpost());
    }

    @Test
    public void testMapInFormatFormatoPEW3CE00ModifyCustomer()throws Exception{
        DTOIntCustomerV2 dtoIntCustomer = dummyMock.getDTOIntCustomer();
        FormatoPEW3CE00 formatInput = mapper.mapInFortmatoPEW3CE00ModifyCustomer(dtoIntCustomer);

        Assert.assertNotNull(formatInput);
        Assert.assertEquals(dtoIntCustomer.getFirstName() ,formatInput.getNombre());
        Assert.assertEquals(dtoIntCustomer.getLastName(),formatInput.getApellid());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getBirthDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFenacim());
        Assert.assertEquals(dtoIntCustomer.getMaritalStatus(),formatInput.getEstaciv());
        Assert.assertEquals(dtoIntCustomer.getGender(),formatInput.getSexo());
        Assert.assertEquals(dtoIntCustomer.getHasPassesAway(),formatInput.getFalleci());
        Assert.assertEquals(dtoIntCustomer.getNumberPersonAssociated(),formatInput.getNumdepe());
        Assert.assertEquals(dtoIntCustomer.getIsPublicPerson(),formatInput.getIndpep());


        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getManager(),formatInput.getIugesto());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getCustomerFlag(),formatInput.getPeyclie());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getPremiumFlag(),formatInput.getMarcvip());
        Assert.assertEquals(dtoIntCustomer.getBankingRelationship().getDebitFlag(),formatInput.getMarcdeb());

        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getId(),formatInput.getIdnives());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getTitle(),formatInput.getNiveest());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCountry().getId(),formatInput.getIdpaini());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCountry().getName(), formatInput.getPaisnac());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getState(),formatInput.getDptonac());
        Assert.assertEquals(dtoIntCustomer.getLevelStudy().getCity(),formatInput.getCiudnac());

        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingtype().getId(),formatInput.getIdtipvi());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingtype().getName(), formatInput.getNomtipv());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getRelationType().getId(), formatInput.getIdrelvi());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getRelationType().getName(), formatInput.getNomrelv());
        Assert.assertEquals(dtoIntCustomer.getLivingPlace().getLivingLevel().toString(),formatInput.getEstrato());

        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getJobTittle(), formatInput.getTitulo());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getIsHolder(),formatInput.getSociemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getTypeEmployee(),formatInput.getCargemp());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getEmploymentInformation().getStartDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFeiniem());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getYearsAntiguaty().toString(), formatInput.getAntigue());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getId(), formatInput.getIdnomem());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getName(), formatInput.getNombemp());
        Assert.assertEquals(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getCode(),formatInput.getCiiu());

        StringBuilder idConta = new StringBuilder();
        StringBuilder idTipco = new StringBuilder();
        StringBuilder nmTipco = new StringBuilder();
        StringBuilder isprefe = new StringBuilder();
        StringBuilder isCheck = new StringBuilder();
        for (DTOIntContactDetail dtoIntContactDetail:dtoIntCustomer.getContactDetail()) {
            idConta.append(dtoIntContactDetail.getId());
            idTipco.append(dtoIntContactDetail.getContactType().getId());
            nmTipco.append(dtoIntContactDetail.getContactType().getName());
            isprefe.append(dtoIntContactDetail.getIsPreferential().equals(true)?"S":"N");
            isCheck.append(dtoIntContactDetail.getIsChecked().equals(true)?"S":"N");
        }
        Assert.assertEquals(idConta.toString(), formatInput.getIdconta());
        Assert.assertEquals(idTipco.toString(), formatInput.getIdtipco());
        Assert.assertEquals(nmTipco.toString(), formatInput.getNmtipco());
        Assert.assertEquals(isprefe.toString(), formatInput.getIsprefe());
        Assert.assertEquals(isCheck.toString(), formatInput.getIscheck());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(0).getContact(),formatInput.getVlrco01());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(1).getContact(),formatInput.getVlrco02());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(2).getContact(),formatInput.getVlrco03());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(3).getContact(),formatInput.getVlrco04());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(4).getContact(),formatInput.getVlrco05());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(5).getContact(),formatInput.getVlrco06());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(6).getContact(),formatInput.getVlrco07());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(7).getContact(),formatInput.getVlrco08());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(8).getContact(),formatInput.getVlrco09());
        Assert.assertEquals(dtoIntCustomer.getContactDetail().get(9).getContact(),formatInput.getVlrco10());


        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentType().getId(), formatInput.getTipidcy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentType().getName(), formatInput.getNomtidy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getDocumentNumber(),formatInput.getNumidcy());
        Assert.assertEquals(dtoIntCustomer.getPartnerInformation().getName(),formatInput.getNomidcy());

        StringBuilder naciona = new StringBuilder();
        naciona.append(dtoIntCustomer.getNationalities().get(0).getId());
        naciona.append(dtoIntCustomer.getNationalities().get(0).getName());
        Assert.assertEquals(naciona.toString(),formatInput.getNaciona());


        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentType().getId(),formatInput.getIdtipdo());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentType().getName(), formatInput.getNomtidc());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getDocumentNumber(),formatInput.getNumidcl());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpeditionDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFeexped());
        Assert.assertEquals(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpirationDate(),DateFormatEnum.YYYY_MM_DD_TIME_FORMAT), formatInput.getFevenci());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getId(), formatInput.getIdpaiex());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getName(), formatInput.getPaisexp());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getStateExpedition(),formatInput.getDptoexp());
        Assert.assertEquals(dtoIntCustomer.getIdentityDocument().getCityExpedition(),formatInput.getCiudexp());

        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getType(),formatInput.getTipdire());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCountry().getId(),formatInput.getIdpaidi());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCountry().getName(), formatInput.getPaisdir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getRegion(),formatInput.getDptodir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getCity(),formatInput.getCiuddir());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getAddressName(),formatInput.getDirecci());
        Assert.assertEquals(dtoIntCustomer.getLegalAddresses().getZipCode(),formatInput.getCodpost());
    }

}
