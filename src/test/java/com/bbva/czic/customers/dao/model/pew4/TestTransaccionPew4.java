package com.bbva.czic.customers.dao.model.pew4;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;

/**
 * Test de la transacci&oacute;n <code>PEW4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPew4 {

	@InjectMocks
	private TransaccionPew4 transaccion;

	@Spy
	private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);
	private static final Log LOG = LogFactory.getLog(TestTransaccionPew4.class);
	
	@Test
	public void test() throws ExcepcionTransaccion {
		
		PeticionTransaccionPew4 peticion = new PeticionTransaccionPew4();

		RespuestaTransaccionPew4 respuesta = new RespuestaTransaccionPew4();

		Mockito.when(
				servicioTransacciones.invocar(PeticionTransaccionPew4.class, RespuestaTransaccionPew4.class, peticion))
				.thenReturn(respuesta);

		RespuestaTransaccionPew4 result = transaccion.invocar(peticion);

		Assert.assertNotNull(result);
		

	}
}