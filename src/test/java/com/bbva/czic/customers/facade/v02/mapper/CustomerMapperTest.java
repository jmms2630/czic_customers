package com.bbva.czic.customers.facade.v02.mapper;

import com.bbva.czic.customers.DummyMock;
import com.bbva.czic.customers.business.dto.DTOIntContactDetail;
import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.business.dto.DTOIntNationalities;
import com.bbva.czic.customers.facade.v02.dto.Customer;
import com.bbva.czic.customers.facade.v02.dto.DataCustomer;
import com.bbva.czic.customers.facade.v02.dto.Nationalities;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class CustomerMapperTest {
    private ICustomerMapper mapper;

    private DummyMock dummyMock;

    @Before
    public void setUp() {
        mapper = new CustomerMapper();
        dummyMock = new DummyMock();
    }

    @Test
    public void testMapOutGetUsers() {

        final DTOIntCustomerV2 dtoIntCustomerV2 = dummyMock.getDTOIntCustomer();

        final DataCustomer dtOut = mapper.customerMap(dtoIntCustomerV2);
         int i =0;
        Customer dtoCustomer = dtOut.getData();

        Assert.assertNotNull(dtOut);
        Assert.assertEquals(dtoCustomer.getCustomerId(), dtoIntCustomerV2.getCustomerId());
        Assert.assertEquals(dtoCustomer.getFirstName(), dtoIntCustomerV2.getFirstName());
        Assert.assertEquals(dtoCustomer.getLastName(), dtoIntCustomerV2.getLastName());
        Assert.assertEquals(dtoCustomer.getBirthDate(),dtoIntCustomerV2.getBirthDate());
        Assert.assertEquals(dtoCustomer.getMaritalStatus(),dtoIntCustomerV2.getMaritalStatus());
        Assert.assertEquals(dtoCustomer.getGender(), dtoIntCustomerV2.getGender());

        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getDocumentType().getId(), dtoIntCustomerV2
                .getPartnerInformation().getDocumentType().getId());
        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getDocumentType().getName(), dtoIntCustomerV2
                .getPartnerInformation().getDocumentType().getName());
        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getDocumentNumber(), dtoIntCustomerV2
                .getPartnerInformation().getDocumentNumber());
        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getName(), dtoIntCustomerV2
                .getPartnerInformation().getName());

        for(final DTOIntNationalities dtoIntNationalities:dtoIntCustomerV2.getNationalities()) {
            Assert.assertEquals(dtoCustomer.getNationalities().get(i).getId(), dtoIntNationalities.getId());
            Assert.assertEquals(dtoCustomer.getNationalities().get(i).getName(), dtoIntNationalities.getName());
            i++;
        }
        i=0;
        Assert.assertEquals(dtoCustomer.getHasPassesAway(),dtoIntCustomerV2.getHasPassesAway());

        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getDocumentType().getId(),dtoIntCustomerV2
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getDocumentType().getName(),dtoIntCustomerV2
                .getIdentityDocument().getDocumentType().getName());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getDocumentNumber(), dtoIntCustomerV2
                .getIdentityDocument().getDocumentNumber());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getExpeditionDate(),dtoIntCustomerV2
                .getIdentityDocument().getExpeditionDate());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getExpirationDate(),dtoIntCustomerV2
                .getIdentityDocument().getExpirationDate());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getCountryExpedition().getId(),dtoIntCustomerV2
                .getIdentityDocument().getCountryExpedition().getId());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getCountryExpedition().getName(),dtoIntCustomerV2
                .getIdentityDocument().getCountryExpedition().getName());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getStateExpedition(),dtoIntCustomerV2
                .getIdentityDocument().getStateExpedition());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getCityExpedition(),dtoIntCustomerV2
                .getIdentityDocument().getCityExpedition());

        Assert.assertEquals(dtoCustomer.getNumberPersonAssociated(),dtoIntCustomerV2.getNumberPersonAssociated());

        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getType(),dtoIntCustomerV2
                .getLegalAddresses().getType());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getAddressName(),dtoIntCustomerV2
                .getLegalAddresses().getAddressName());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getCountry().getId(),dtoIntCustomerV2
                .getLegalAddresses().getCountry().getId());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getCountry().getName(),dtoIntCustomerV2
                .getLegalAddresses().getCountry().getName());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getRegion(),dtoIntCustomerV2
                .getLegalAddresses().getRegion());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getCity(),dtoIntCustomerV2
                .getLegalAddresses().getCity());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getZipCode(),dtoIntCustomerV2
                .getLegalAddresses().getZipCode());

        for (final DTOIntContactDetail dtoIntContactDetail:dtoIntCustomerV2.getContactDetail()) {
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getId(), dtoIntContactDetail.getId());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getContact(), dtoIntContactDetail.getContact());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getContactType().getId(), dtoIntContactDetail.getContactType().getId());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getContactType().getName(), dtoIntContactDetail.getContactType().getName());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getIsChecked(),dtoIntContactDetail.getIsChecked());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getIsPreferential(),dtoIntContactDetail.getIsPreferential());
            i++;
        }
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getCustomerFlag(),dtoIntCustomerV2
                .getBankingRelationship().getCustomerFlag());
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getDebitFlag(),dtoIntCustomerV2
                .getBankingRelationship().getDebitFlag());
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getPremiumFlag(),dtoIntCustomerV2
                .getBankingRelationship().getPremiumFlag());
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getManager(),dtoIntCustomerV2
                .getBankingRelationship().getManager());

        Assert.assertEquals(dtoCustomer.getLevelStudy().getId(),dtoIntCustomerV2.getLevelStudy().getId());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getTitle(),dtoIntCustomerV2.getLevelStudy().getTitle());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getCountry().getId(),dtoIntCustomerV2.getLevelStudy().getCountry().getId());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getCountry().getName(),dtoIntCustomerV2.getLevelStudy().getCountry().getName());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getState(),dtoIntCustomerV2.getLevelStudy().getState());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getCity(),dtoIntCustomerV2.getLevelStudy().getCity());

        Assert.assertEquals(dtoCustomer.getLivingPlace().getLivingtype().getId(),dtoIntCustomerV2.getLivingPlace().getLivingtype().getId());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getLivingtype().getName(),dtoIntCustomerV2.getLivingPlace().getLivingtype().getName());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getRelationType().getId(),dtoIntCustomerV2.getLivingPlace().getRelationType().getId());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getRelationType().getName(),dtoIntCustomerV2.getLivingPlace().getRelationType().getName());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getLivingLevel(),dtoIntCustomerV2.getLivingPlace().getLivingLevel());

        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getTypeEmployee(),dtoIntCustomerV2.getEmploymentInformation().getTypeEmployee());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getIsHolder(),dtoIntCustomerV2.getEmploymentInformation().getIsHolder());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getJobTittle(),dtoIntCustomerV2.getEmploymentInformation().getJobTittle());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getStartDate(),dtoIntCustomerV2.getEmploymentInformation().getStartDate());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getYearsAntiguaty(),dtoIntCustomerV2.getEmploymentInformation().getYearsAntiguaty());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getPurpouseCompany().getId(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getId());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getPurpouseCompany().getName(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getName());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getPurpouseCompany().getCode(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getCode());

        Assert.assertEquals(dtoCustomer.getIsPublicPerson(),dtoIntCustomerV2.getIsPublicPerson());

    }

    @Test
    public void testMapInCreateCustomer(){


        final DataCustomer dtOut = dummyMock.CreateMapInCustomer();
        int i =0;
        Customer dtoCustomer = dtOut.getData();
        final DTOIntCustomerV2 dtoIntCustomerV2 = mapper.mapInCreateCustomer(dtoCustomer);

        Assert.assertNotNull(dtoIntCustomerV2);
        Assert.assertEquals(dtoCustomer.getCustomerId(), dtoIntCustomerV2.getCustomerId());
        Assert.assertEquals(dtoCustomer.getFirstName(), dtoIntCustomerV2.getFirstName());
        Assert.assertEquals(dtoCustomer.getLastName(), dtoIntCustomerV2.getLastName());
        Assert.assertEquals(dtoCustomer.getBirthDate(),dtoIntCustomerV2.getBirthDate());
        Assert.assertEquals(dtoCustomer.getMaritalStatus(),dtoIntCustomerV2.getMaritalStatus());
        Assert.assertEquals(dtoCustomer.getGender(), dtoIntCustomerV2.getGender());

        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getDocumentType().getId(), dtoIntCustomerV2
                .getPartnerInformation().getDocumentType().getId());
        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getDocumentType().getName(), dtoIntCustomerV2
                .getPartnerInformation().getDocumentType().getName());
        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getDocumentNumber(), dtoIntCustomerV2
                .getPartnerInformation().getDocumentNumber());
        Assert.assertEquals(dtoCustomer.getPartnerInformation().get(0).getName(), dtoIntCustomerV2
                .getPartnerInformation().getName());

        for(final Nationalities nationalities:dtoCustomer.getNationalities()) {
            Assert.assertEquals(dtoIntCustomerV2.getNationalities().get(i).getId(), nationalities.getId());
            Assert.assertEquals(dtoIntCustomerV2.getNationalities().get(i).getName(), nationalities.getName());
            i++;
        }
        i=0;
        Assert.assertEquals(dtoCustomer.getHasPassesAway(),dtoIntCustomerV2.getHasPassesAway());

        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getDocumentType().getId(),dtoIntCustomerV2
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getDocumentType().getName(),dtoIntCustomerV2
                .getIdentityDocument().getDocumentType().getName());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getDocumentNumber(), dtoIntCustomerV2
                .getIdentityDocument().getDocumentNumber());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getExpeditionDate(),dtoIntCustomerV2
                .getIdentityDocument().getExpeditionDate());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getExpirationDate(),dtoIntCustomerV2
                .getIdentityDocument().getExpirationDate());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getCountryExpedition().getId(),dtoIntCustomerV2
                .getIdentityDocument().getCountryExpedition().getId());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getCountryExpedition().getName(),dtoIntCustomerV2
                .getIdentityDocument().getCountryExpedition().getName());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getStateExpedition(),dtoIntCustomerV2
                .getIdentityDocument().getStateExpedition());
        Assert.assertEquals(dtoCustomer.getIdentityDocuments().get(0).getCityExpedition(),dtoIntCustomerV2
                .getIdentityDocument().getCityExpedition());

        Assert.assertEquals(dtoCustomer.getNumberPersonAssociated(),dtoIntCustomerV2.getNumberPersonAssociated());

        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getType(),dtoIntCustomerV2
                .getLegalAddresses().getType());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getAddressName(),dtoIntCustomerV2
                .getLegalAddresses().getAddressName());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getCountry().getId(),dtoIntCustomerV2
                .getLegalAddresses().getCountry().getId());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getCountry().getName(),dtoIntCustomerV2
                .getLegalAddresses().getCountry().getName());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getRegion(),dtoIntCustomerV2
                .getLegalAddresses().getRegion());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getCity(),dtoIntCustomerV2
                .getLegalAddresses().getCity());
        Assert.assertEquals(dtoCustomer.getLegalAddresses().get(0).getZipCode(),dtoIntCustomerV2
                .getLegalAddresses().getZipCode());

        for (final DTOIntContactDetail dtoIntContactDetail:dtoIntCustomerV2.getContactDetail()) {
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getId(), dtoIntContactDetail.getId());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getContact(), dtoIntContactDetail.getContact());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getContactType().getId(), dtoIntContactDetail.getContactType().getId());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getContactType().getName(), dtoIntContactDetail.getContactType().getName());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getIsChecked(),dtoIntContactDetail.getIsChecked());
            Assert.assertEquals(dtoCustomer.getContactDetails().get(i).getIsPreferential(),dtoIntContactDetail.getIsPreferential());
            i++;
        }
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getCustomerFlag(),dtoIntCustomerV2
                .getBankingRelationship().getCustomerFlag());
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getDebitFlag(),dtoIntCustomerV2
                .getBankingRelationship().getDebitFlag());
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getPremiumFlag(),dtoIntCustomerV2
                .getBankingRelationship().getPremiumFlag());
        Assert.assertEquals(dtoCustomer.getBankingRelationship().getManager(),dtoIntCustomerV2
                .getBankingRelationship().getManager());

        Assert.assertEquals(dtoCustomer.getLevelStudy().getId(),dtoIntCustomerV2.getLevelStudy().getId());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getTitle(),dtoIntCustomerV2.getLevelStudy().getTitle());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getCountry().getId(),dtoIntCustomerV2.getLevelStudy().getCountry().getId());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getCountry().getName(),dtoIntCustomerV2.getLevelStudy().getCountry().getName());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getState(),dtoIntCustomerV2.getLevelStudy().getState());
        Assert.assertEquals(dtoCustomer.getLevelStudy().getCity(),dtoIntCustomerV2.getLevelStudy().getCity());

        Assert.assertEquals(dtoCustomer.getLivingPlace().getLivingtype().getId(),dtoIntCustomerV2.getLivingPlace().getLivingtype().getId());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getLivingtype().getName(),dtoIntCustomerV2.getLivingPlace().getLivingtype().getName());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getRelationType().getId(),dtoIntCustomerV2.getLivingPlace().getRelationType().getId());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getRelationType().getName(),dtoIntCustomerV2.getLivingPlace().getRelationType().getName());
        Assert.assertEquals(dtoCustomer.getLivingPlace().getLivingLevel(),dtoIntCustomerV2.getLivingPlace().getLivingLevel());

        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getTypeEmployee(),dtoIntCustomerV2.getEmploymentInformation().getTypeEmployee());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getIsHolder(),dtoIntCustomerV2.getEmploymentInformation().getIsHolder());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getJobTittle(),dtoIntCustomerV2.getEmploymentInformation().getJobTittle());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getStartDate(),dtoIntCustomerV2.getEmploymentInformation().getStartDate());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getYearsAntiguaty(),dtoIntCustomerV2.getEmploymentInformation().getYearsAntiguaty());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getPurpouseCompany().getId(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getId());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getPurpouseCompany().getName(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getName());
        Assert.assertEquals(dtoCustomer.getEmploymentInformation().getPurpouseCompany().getCode(),dtoIntCustomerV2.getEmploymentInformation().getPurpouseCompany().getCode());

        Assert.assertEquals(dtoCustomer.getIsPublicPerson(),dtoIntCustomerV2.getIsPublicPerson());

    }

    @Test
    public void testMapInModifyCustomer(){
        testMapInCreateCustomer();
    }

}
