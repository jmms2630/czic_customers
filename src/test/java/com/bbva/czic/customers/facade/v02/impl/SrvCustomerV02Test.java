package com.bbva.czic.customers.facade.v02.impl;

import com.bbva.czic.customers.DummyMock;
import com.bbva.czic.customers.facade.v02.dto.DataCustomer;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;
import com.bbva.zic.commons.rm.utils.errors.EnumError;
import com.bbva.zic.utilTest.UriInfoImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.ws.rs.core.Response;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = BusinessServiceTestContextLoader.class, locations = {
        "classpath*:/META-INF/spring/applicationContext-*.xml", "classpath:/META-INF/spring/business-service.xml",
        "classpath:/META-INF/spring/business-service-test.xml" })
@TestExecutionListeners(listeners = { MockInvocationContextTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class })
@ActiveProfiles("dev")
/**
 * Created by Entelgy on 19/08/2016.
 */
public class SrvCustomerV02Test {
    @Autowired
    private SrvCustomersV02 srv;

    private DummyMock dummyMock;


    @Before
    public void init() {
        srv.setUriInfo(new UriInfoImpl());
        dummyMock= new DummyMock();
    }

    @Test
    public void testGetCustomerSrvIntEmptyCustomerId(){
        try {
            srv.getCustomer(" ");

            Assert.fail();

        } catch (final BusinessServiceException e) {
            Assert.assertEquals(e.getErrorCode(), EnumError.MANDATORY_PARAMETERES_MISSING.getAlias());
        }
    }

    @Test
    public void callGetCustomerWhitIdNullTest() {

        try {
            srv.getCustomer(null);

            Assert.fail();

        } catch (final BusinessServiceException e) {
            Assert.assertEquals(e.getErrorCode(), EnumError.TECHNICAL_ERROR.getAlias());
        }
    }

    @Test
    public void getCustomerBirthdateNull() {

        final DataCustomer customer = srv.getCustomer("30");

        Assert.assertNotNull(customer);
    }

    @Test
    public void getCustomerContactDetailsNull() {

        final DataCustomer customer = srv.getCustomer("20");

        Assert.assertNotNull(customer);
    }

    @Test
    public void getCustomerNationalityNull() {

        final DataCustomer customer = srv.getCustomer("10");

        Assert.assertNotNull(customer);
    }

    @Test
    public void getCustomerOkTest() {

        final DataCustomer customer = srv.getCustomer("0");

        Assert.assertNotNull(customer);
    }

    @Test
    public void createCustomerOkTest() {
        final DataCustomer customer = dummyMock.CreateMapInCustomer();
        final String customerNumber = srv.createCustomers(customer.getData());
        Assert.assertNotNull(customerNumber);
    }

    @Test
    public void modifyCustomerOkTest() {
        final DataCustomer customer = dummyMock.CreateMapInCustomer();
        final Response r= srv.modifyCustomer("12345", customer.getData());
        Assert.assertNotNull(r);
    }

    private BusinessServiceException getBsnExeptionByAlias(String alias) {
        return new BusinessServiceException(alias);
    }
}
