package com.bbva.czic.customers.dao.tx;

import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.dao.mappers.ITxCustomrMapper;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CE00;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CS00;
import com.bbva.czic.customers.dao.model.pew4.PeticionTransaccionPew4;
import com.bbva.czic.customers.dao.model.pew4.RespuestaTransaccionPew4;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.zic.commons.rm.utils.tx.impl.SimpleBbvaTransaction;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Entelgy on 17/08/2016.
 */
@Component(value = "txCreateCustomer")
public class TxCreateCustomer extends SimpleBbvaTransaction<DTOIntCustomerV2,FormatoPEW4CE00,String,FormatoPEW4CS00> {

    @Resource(name="transaccionPew4")
    private transient InvocadorTransaccion<PeticionTransaccionPew4, RespuestaTransaccionPew4> transaccionPew4;

    @Resource(name = "txCustomrMapper")
    private ITxCustomrMapper txCustomrMapper;
    @Override
    protected FormatoPEW4CE00 mapDtoInToRequestFormat(DTOIntCustomerV2 dtoIntCustomerV2) {
        return txCustomrMapper.mapInFortmatoPEW4CE00CreateCustomer(dtoIntCustomerV2);
    }

    @Override
    protected String mapResponseFormatToDtoOut(FormatoPEW4CS00 formatoPEW4CS00, DTOIntCustomerV2 dtoIntCustomer) {
        return txCustomrMapper.mapOutFormatoPEW4CS00CreateCustomer(formatoPEW4CS00);
    }

    @Override
    protected InvocadorTransaccion<?, ?> getTransaction() {
        return transaccionPew4;
    }
}
