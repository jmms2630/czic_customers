// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.czic.customers.dao.model.pew4;

import java.lang.String;

privileged aspect FormatoPEW4CE00_Roo_ToString {
    
    public String FormatoPEW4CE00.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Antigue: ").append(getAntigue()).append(", ");
        sb.append("Apellid: ").append(getApellid()).append(", ");
        sb.append("Cargemp: ").append(getCargemp()).append(", ");
        sb.append("Ciiu: ").append(getCiiu()).append(", ");
        sb.append("Ciuddir: ").append(getCiuddir()).append(", ");
        sb.append("Ciudexp: ").append(getCiudexp()).append(", ");
        sb.append("Ciudnac: ").append(getCiudnac()).append(", ");
        sb.append("Codpost: ").append(getCodpost()).append(", ");
        sb.append("Direcci: ").append(getDirecci()).append(", ");
        sb.append("Dptodir: ").append(getDptodir()).append(", ");
        sb.append("Dptoexp: ").append(getDptoexp()).append(", ");
        sb.append("Dptonac: ").append(getDptonac()).append(", ");
        sb.append("Estaciv: ").append(getEstaciv()).append(", ");
        sb.append("Estrato: ").append(getEstrato()).append(", ");
        sb.append("Falleci: ").append(getFalleci()).append(", ");
        sb.append("Feexped: ").append(getFeexped()).append(", ");
        sb.append("Feiniem: ").append(getFeiniem()).append(", ");
        sb.append("Fenacim: ").append(getFenacim()).append(", ");
        sb.append("Fevenci: ").append(getFevenci()).append(", ");
        sb.append("Idclien: ").append(getIdclien()).append(", ");
        sb.append("Idconta: ").append(getIdconta()).append(", ");
        sb.append("Idnives: ").append(getIdnives()).append(", ");
        sb.append("Idnomem: ").append(getIdnomem()).append(", ");
        sb.append("Idpaidi: ").append(getIdpaidi()).append(", ");
        sb.append("Idpaiex: ").append(getIdpaiex()).append(", ");
        sb.append("Idpaini: ").append(getIdpaini()).append(", ");
        sb.append("Idrelvi: ").append(getIdrelvi()).append(", ");
        sb.append("Idtipco: ").append(getIdtipco()).append(", ");
        sb.append("Idtipdo: ").append(getIdtipdo()).append(", ");
        sb.append("Idtipvi: ").append(getIdtipvi()).append(", ");
        sb.append("Indpep: ").append(getIndpep()).append(", ");
        sb.append("Ischeck: ").append(getIscheck()).append(", ");
        sb.append("Isprefe: ").append(getIsprefe()).append(", ");
        sb.append("Iugesto: ").append(getIugesto()).append(", ");
        sb.append("Marcdeb: ").append(getMarcdeb()).append(", ");
        sb.append("Marcvip: ").append(getMarcvip()).append(", ");
        sb.append("Naciona: ").append(getNaciona()).append(", ");
        sb.append("Niveest: ").append(getNiveest()).append(", ");
        sb.append("Nmtipco: ").append(getNmtipco()).append(", ");
        sb.append("Nombemp: ").append(getNombemp()).append(", ");
        sb.append("Nombre: ").append(getNombre()).append(", ");
        sb.append("Nomidcy: ").append(getNomidcy()).append(", ");
        sb.append("Nomrelv: ").append(getNomrelv()).append(", ");
        sb.append("Nomtidc: ").append(getNomtidc()).append(", ");
        sb.append("Nomtidy: ").append(getNomtidy()).append(", ");
        sb.append("Nomtipv: ").append(getNomtipv()).append(", ");
        sb.append("Numdepe: ").append(getNumdepe()).append(", ");
        sb.append("Numidcl: ").append(getNumidcl()).append(", ");
        sb.append("Numidcy: ").append(getNumidcy()).append(", ");
        sb.append("Paisdir: ").append(getPaisdir()).append(", ");
        sb.append("Paisexp: ").append(getPaisexp()).append(", ");
        sb.append("Paisnac: ").append(getPaisnac()).append(", ");
        sb.append("Peyclie: ").append(getPeyclie()).append(", ");
        sb.append("Sexo: ").append(getSexo()).append(", ");
        sb.append("Sociemp: ").append(getSociemp()).append(", ");
        sb.append("Tipdire: ").append(getTipdire()).append(", ");
        sb.append("Tipidcy: ").append(getTipidcy()).append(", ");
        sb.append("Titulo: ").append(getTitulo()).append(", ");
        sb.append("Vlrco01: ").append(getVlrco01()).append(", ");
        sb.append("Vlrco02: ").append(getVlrco02()).append(", ");
        sb.append("Vlrco03: ").append(getVlrco03()).append(", ");
        sb.append("Vlrco04: ").append(getVlrco04()).append(", ");
        sb.append("Vlrco05: ").append(getVlrco05()).append(", ");
        sb.append("Vlrco06: ").append(getVlrco06()).append(", ");
        sb.append("Vlrco07: ").append(getVlrco07()).append(", ");
        sb.append("Vlrco08: ").append(getVlrco08()).append(", ");
        sb.append("Vlrco09: ").append(getVlrco09()).append(", ");
        sb.append("Vlrco10: ").append(getVlrco10());
        return sb.toString();
    }
    
}
