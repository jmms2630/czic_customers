package com.bbva.czic.customers.dao.model.pew3.mock;


import com.bbva.czic.customers.dao.model.pew3.PeticionTransaccionPew3;
import com.bbva.czic.customers.dao.model.pew3.RespuestaTransaccionPew3;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>PEW3</code>
 *
 * @see com.bbva.czic.customers.dao.model.pew3.PeticionTransaccionPew3
 * @see com.bbva.czic.customers.dao.model.pew3.RespuestaTransaccionPew3
 */
@Component(value = "transaccionPew3")
@Profile(value = "dev")
public class TransaccionPew3Mock implements InvocadorTransaccion<PeticionTransaccionPew3, RespuestaTransaccionPew3> {

    @Override
    public RespuestaTransaccionPew3 invocar(final PeticionTransaccionPew3 transaccion) throws ExcepcionTransaccion {
        return new RespuestaTransaccionPew3();
    }

    @Override
    public RespuestaTransaccionPew3 invocarCache(final PeticionTransaccionPew3 transaccion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
    }
}