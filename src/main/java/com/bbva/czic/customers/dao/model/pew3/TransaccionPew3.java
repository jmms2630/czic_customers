package com.bbva.czic.customers.dao.model.pew3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PEW3</code>
 * 
 * @see PeticionTransaccionPew3
 * @see RespuestaTransaccionPew3
 */
@Component(value = "transaccionPew3")
@Profile(value = "prod")
public class TransaccionPew3 implements InvocadorTransaccion<PeticionTransaccionPew3,RespuestaTransaccionPew3> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPew3 invocar(PeticionTransaccionPew3 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPew3.class, RespuestaTransaccionPew3.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPew3 invocarCache(PeticionTransaccionPew3 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPew3.class, RespuestaTransaccionPew3.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}