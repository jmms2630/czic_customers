package com.bbva.czic.customers.dao.mappers;

import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.dao.model.pew2.*;
import com.bbva.czic.customers.dao.model.pew3.FormatoPEW3CE00;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CE00;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CS00;

/**
 * Created by Entelgy on 12/08/2016.
 */
public interface ITxCustomrMapper {

    FormatoPEW2CE00 mapInFormatoPEW2CE00GetCustomer(String dtoIn);

    DTOIntCustomerV2 mapOutFormatoPEW2CS00GetCustomer(FormatoPEW2CS00 formatOutput,DTOIntCustomerV2 dtoIntCustomer);

    DTOIntCustomerV2 mapOutFormatoPEW2CS01GetCustomer(FormatoPEW2CS01 formatOutput,DTOIntCustomerV2 dtoIntCustomer);

    DTOIntCustomerV2 mapOutFormatoPEW2CS02GetCustomer(FormatoPEW2CS02 formatOutput,DTOIntCustomerV2 dtoIntCustomer);

    DTOIntCustomerV2 mapOutFormatoPEW2CS03GetCustomer(FormatoPEW2CS03 formatOutput,DTOIntCustomerV2 dtoIntCustomer);

    DTOIntCustomerV2 mapOutFormatoPEW2CS04GetCustomer(FormatoPEW2CS04 formatOutput,DTOIntCustomerV2 dtoIntCustomer);

    DTOIntCustomerV2 mapOutFormatoPEW2CS05GetCustomer(FormatoPEW2CS05 formatOutput,DTOIntCustomerV2 dtoIntCustomer);

    FormatoPEW4CE00 mapInFortmatoPEW4CE00CreateCustomer(DTOIntCustomerV2 dtoIntCustomerV2);

    String mapOutFormatoPEW4CS00CreateCustomer(FormatoPEW4CS00 formatoPEW4CS00);

    FormatoPEW3CE00 mapInFortmatoPEW3CE00ModifyCustomer(DTOIntCustomerV2 dtoIntCustomerV2);
}
