package com.bbva.czic.customers.dao.mappers.impl;

import com.bbva.ccol.commons.rm.utils.converters.DateConverter;
import com.bbva.ccol.commons.rm.utils.converters.DateFormatEnum;
import com.bbva.czic.customers.business.dto.*;
import com.bbva.czic.customers.dao.mappers.ITxCustomrMapper;
import com.bbva.czic.customers.dao.model.pew2.*;
import com.bbva.czic.customers.dao.model.pew3.FormatoPEW3CE00;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CE00;
import com.bbva.czic.customers.dao.model.pew4.FormatoPEW4CS00;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Entelgy on 12/08/2016.
 */
@Component(value = "txCustomrMapper")
public class TxCustomrMapper implements ITxCustomrMapper {

    @Override
    public FormatoPEW2CE00 mapInFormatoPEW2CE00GetCustomer(String dtoIn) {
        FormatoPEW2CE00 formatoPEW2CE00 = new FormatoPEW2CE00();
        formatoPEW2CE00.setIdclien(dtoIn);
        return formatoPEW2CE00;
    }

    @Override
    public DTOIntCustomerV2 mapOutFormatoPEW2CS00GetCustomer(FormatoPEW2CS00 formatOutput,DTOIntCustomerV2 dtoIntCustomer){
        dtoIntCustomer.setFirstName(formatOutput.getNombre());
        dtoIntCustomer.setLastName(formatOutput.getApellid());
        dtoIntCustomer.setBirthDate(new DateConverter().toDate(formatOutput.getFenacim(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        dtoIntCustomer.setMaritalStatus(formatOutput.getEstaciv());
        dtoIntCustomer.setGender(formatOutput.getSexo());
        dtoIntCustomer.setHasPassesAway(formatOutput.getFalleci());
        dtoIntCustomer.setNumberPersonAssociated(formatOutput.getNumdepe());

        final DTOIntBankingRelationship dtoIntBankingRelationship = new DTOIntBankingRelationship();
        dtoIntBankingRelationship.setManager(formatOutput.getIugesto());
        dtoIntBankingRelationship.setCustomerFlag(formatOutput.getPeyclie());
        dtoIntBankingRelationship.setPremiumFlag(formatOutput.getMarcvip());
        dtoIntBankingRelationship.setDebitFlag(formatOutput.getMarcdeb());
        dtoIntCustomer.setBankingRelationship(dtoIntBankingRelationship);

        final DTOIntLevelStudy dtoIntLevelStudy =  new DTOIntLevelStudy();
        dtoIntLevelStudy.setId(formatOutput.getIdnives());
        dtoIntLevelStudy.setTitle(formatOutput.getNiveest());
        final DTOIntCountry dtoIntCountry = new DTOIntCountry();
        dtoIntCountry.setId(formatOutput.getIdpaini());
        dtoIntCountry.setName(formatOutput.getPaisnac());
        dtoIntLevelStudy.setCountry(dtoIntCountry);
        dtoIntLevelStudy.setState(formatOutput.getDptonac());
        dtoIntLevelStudy.setCity(formatOutput.getCiudnac());
        dtoIntCustomer.setLevelStudy(dtoIntLevelStudy);

        final DTOIntLivingPlace dtoIntLivingPlace = new DTOIntLivingPlace();
        DTOIntType dtoIntType = new DTOIntType();
        dtoIntType.setId(formatOutput.getIdtipvi());
        dtoIntType.setName(formatOutput.getNomtipv());
        dtoIntLivingPlace.setLivingtype(dtoIntType);
        dtoIntType = new DTOIntType();
        dtoIntType.setId(formatOutput.getIdrelvi());
        dtoIntType.setName(formatOutput.getNomrelv());
        dtoIntLivingPlace.setRelationType(dtoIntType);
        dtoIntLivingPlace.setLivingLevel(Integer.valueOf(formatOutput.getEstrato()));
        dtoIntCustomer.setLivingPlace(dtoIntLivingPlace);

        final DTOIntEmploymentInformation dtoIntEmploymentInformation = new DTOIntEmploymentInformation();
        dtoIntEmploymentInformation.setJobTittle(formatOutput.getTitulo());
        dtoIntEmploymentInformation.setIsHolder(formatOutput.getSociemp());
        dtoIntEmploymentInformation.setTypeEmployee(formatOutput.getCargemp());
        dtoIntEmploymentInformation.setStartDate(new DateConverter().toDate(formatOutput.getFeiniem(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        dtoIntEmploymentInformation.setYearsAntiguaty(Integer.valueOf(formatOutput.getAntigue()));
        final DTOIntPurpouseCompany dtoIntPurpouseCompany = new DTOIntPurpouseCompany();
        dtoIntPurpouseCompany.setId(formatOutput.getIdnomem());
        dtoIntPurpouseCompany.setName(formatOutput.getNombemp());
        dtoIntPurpouseCompany.setCode(formatOutput.getCiiu());
        dtoIntEmploymentInformation.setPurpouseCompany(dtoIntPurpouseCompany);
        dtoIntCustomer.setEmploymentInformation(dtoIntEmploymentInformation);

        dtoIntCustomer.setIsPublicPerson(formatOutput.getIndpep());


        return dtoIntCustomer;
    }

    @Override
    public DTOIntCustomerV2 mapOutFormatoPEW2CS01GetCustomer(FormatoPEW2CS01 formatOutput,DTOIntCustomerV2 dtoIntCustomer) {
        final List<DTOIntContactDetail> dtoIntContactDetails = new ArrayList<DTOIntContactDetail>();
        DTOIntContactDetail dtoIntContactDetail = new DTOIntContactDetail();
        DTOIntType dtoIntType = new DTOIntType();


        dtoIntContactDetail.setId(formatOutput.getIdcon01());
        dtoIntContactDetail.setContact(formatOutput.getVlrco01());
        dtoIntType.setId(formatOutput.getIdtic01());
        dtoIntType.setName(formatOutput.getTipco01());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf01(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc01(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail= new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon02());
        dtoIntContactDetail.setContact(formatOutput.getVlrco02());
        dtoIntType.setId(formatOutput.getIdtic02());
        dtoIntType.setName(formatOutput.getTipco02());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf02(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc02(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon03());
        dtoIntContactDetail.setContact(formatOutput.getVlrco03());
        dtoIntType.setId(formatOutput.getIdtic03());
        dtoIntType.setName(formatOutput.getTipco03());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf03(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc03(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon04());
        dtoIntContactDetail.setContact(formatOutput.getVlrco04());
        dtoIntType.setId(formatOutput.getIdtic04());
        dtoIntType.setName(formatOutput.getTipco04());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf04(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc04(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon05());
        dtoIntContactDetail.setContact(formatOutput.getVlrco05());
        dtoIntType.setId(formatOutput.getIdtic05());
        dtoIntType.setName(formatOutput.getTipco05());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf05(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc05(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon06());
        dtoIntContactDetail.setContact(formatOutput.getVlrco06());
        dtoIntType.setId(formatOutput.getIdtic06());
        dtoIntType.setName(formatOutput.getTipco06());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf06(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc06(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon07());
        dtoIntContactDetail.setContact(formatOutput.getVlrco07());
        dtoIntType.setId(formatOutput.getIdtic07());
        dtoIntType.setName(formatOutput.getTipco07());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf07(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc07(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon08());
        dtoIntContactDetail.setContact(formatOutput.getVlrco08());
        dtoIntType.setId(formatOutput.getIdtic08());
        dtoIntType.setName(formatOutput.getTipco08());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf08(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc08(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon09());
        dtoIntContactDetail.setContact(formatOutput.getVlrco09());
        dtoIntType.setId(formatOutput.getIdtic09());
        dtoIntType.setName(formatOutput.getTipco09());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf09(), "S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc09(), "S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntContactDetail=new DTOIntContactDetail();
        dtoIntType= new DTOIntType();
        dtoIntContactDetail.setId(formatOutput.getIdcon10());
        dtoIntContactDetail.setContact(formatOutput.getVlrco10());
        dtoIntType.setId(formatOutput.getIdtic10());
        dtoIntType.setName(formatOutput.getTipco10());
        dtoIntContactDetail.setContactType(dtoIntType);
        dtoIntContactDetail.setIsPreferential(StringUtils.equalsIgnoreCase(formatOutput.getEsprf10(),"S"));
        dtoIntContactDetail.setIsChecked(StringUtils.equalsIgnoreCase(formatOutput.getEsmrc10(),"S"));
        dtoIntContactDetails.add(dtoIntContactDetail);

        dtoIntCustomer.setContactDetail(dtoIntContactDetails);

        return dtoIntCustomer;


    }

    @Override
    public DTOIntCustomerV2 mapOutFormatoPEW2CS02GetCustomer(FormatoPEW2CS02 formatOutput,DTOIntCustomerV2 dtoIntCustomer) {
        final DTOIntPartnerInformation dtoIntPartnerInformation = new DTOIntPartnerInformation();
        final DTOIntType dtoIntType = new DTOIntType();
        dtoIntType.setId(formatOutput.getTipidcy());
        dtoIntType.setName(formatOutput.getNomtidy());
        dtoIntPartnerInformation.setDocumentType(dtoIntType);
        dtoIntPartnerInformation.setDocumentNumber(formatOutput.getNumidcy());
        dtoIntPartnerInformation.setName(formatOutput.getNomidcy());
        dtoIntCustomer.setPartnerInformation(dtoIntPartnerInformation);
        return dtoIntCustomer;
    }

    @Override
    public DTOIntCustomerV2 mapOutFormatoPEW2CS03GetCustomer(FormatoPEW2CS03 formatOutput,DTOIntCustomerV2 dtoIntCustomer) {
        final List<DTOIntNationalities> dtoIntNationalities = new ArrayList<DTOIntNationalities>();
        DTOIntNationalities dtoIntNationality = new DTOIntNationalities();
        dtoIntNationality.setId(formatOutput.getIdnaci1());
        dtoIntNationality.setName(formatOutput.getNacion1());
        dtoIntNationalities.add(dtoIntNationality);

        dtoIntNationality = new DTOIntNationalities();
        dtoIntNationality.setId(formatOutput.getIdnaci2());
        dtoIntNationality.setName(formatOutput.getNacion2());
        dtoIntNationalities.add(dtoIntNationality);

        dtoIntNationality = new DTOIntNationalities();
        dtoIntNationality.setId(formatOutput.getIdnaci3());
        dtoIntNationality.setName(formatOutput.getNacion3());
        dtoIntNationalities.add(dtoIntNationality);

        dtoIntNationality = new DTOIntNationalities();
        dtoIntNationality.setId(formatOutput.getIdnaci4());
        dtoIntNationality.setName(formatOutput.getNacion4());
        dtoIntNationalities.add(dtoIntNationality);

        dtoIntCustomer.setNationalities(dtoIntNationalities);


        return dtoIntCustomer;
    }

    @Override
    public DTOIntCustomerV2 mapOutFormatoPEW2CS04GetCustomer(FormatoPEW2CS04 formatOutput,DTOIntCustomerV2 dtoIntCustomer) {
        DTOIntIdentityDocuments dtoIntIdentityDocuments = new DTOIntIdentityDocuments();
        DTOIntType dtoIntType = new DTOIntType();
        dtoIntType.setId(formatOutput.getIdtipdo());
        dtoIntType.setName(formatOutput.getNomtidc());
        dtoIntIdentityDocuments.setDocumentType(dtoIntType);
        dtoIntIdentityDocuments.setDocumentNumber(formatOutput.getNumidcl());
        dtoIntIdentityDocuments.setExpeditionDate(new DateConverter().toDate(formatOutput.getFeexped(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        dtoIntIdentityDocuments.setExpirationDate(new DateConverter().toDate(formatOutput.getFevenci(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        DTOIntCountry dtoIntCountry = new DTOIntCountry();
        dtoIntCountry.setId(formatOutput.getIdpaiex());
        dtoIntCountry.setName(formatOutput.getPaisexp());
        dtoIntIdentityDocuments.setCountryExpedition(dtoIntCountry);
        dtoIntIdentityDocuments.setStateExpedition(formatOutput.getDptoexp());
        dtoIntIdentityDocuments.setCityExpedition(formatOutput.getCiudexp());
        dtoIntCustomer.setIdentityDocument(dtoIntIdentityDocuments);
        return dtoIntCustomer;
    }

    @Override
    public DTOIntCustomerV2 mapOutFormatoPEW2CS05GetCustomer(FormatoPEW2CS05 formatOutput,DTOIntCustomerV2 dtoIntCustomer) {
        DTOIntLegalAddresses dtoIntLegalAddresses = new DTOIntLegalAddresses();
        DTOIntCountry dtoIntCountry = new DTOIntCountry();
        dtoIntLegalAddresses.setType(formatOutput.getTipdire());
        dtoIntCountry.setId(formatOutput.getIdpaidi());
        dtoIntCountry.setName(formatOutput.getPaisdir());
        dtoIntLegalAddresses.setCountry(dtoIntCountry);
        dtoIntLegalAddresses.setRegion(formatOutput.getDptodir());
        dtoIntLegalAddresses.setCity(formatOutput.getCiuddir());
        dtoIntLegalAddresses.setAddressName(formatOutput.getDirecci());
        dtoIntLegalAddresses.setZipCode(formatOutput.getCodpost());
        dtoIntCustomer.setLegalAddresses(dtoIntLegalAddresses);
        return dtoIntCustomer;
    }

    @Override
    public FormatoPEW4CE00 mapInFortmatoPEW4CE00CreateCustomer(DTOIntCustomerV2 dtoIntCustomer) {
        final FormatoPEW4CE00 formatoPEW4CE00 = new FormatoPEW4CE00();
        formatoPEW4CE00.setIdclien(dtoIntCustomer.getCustomerId());
        formatoPEW4CE00.setNombre(dtoIntCustomer.getFirstName());
        formatoPEW4CE00.setApellid(dtoIntCustomer.getLastName());
        formatoPEW4CE00.setFenacim(new DateConverter().toDateFormat(dtoIntCustomer.getBirthDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW4CE00.setEstaciv(dtoIntCustomer.getMaritalStatus());
        formatoPEW4CE00.setSexo(dtoIntCustomer.getGender());

        formatoPEW4CE00.setTipidcy(dtoIntCustomer.getPartnerInformation().getDocumentType().getId());
        formatoPEW4CE00.setNomtidy(dtoIntCustomer.getPartnerInformation().getDocumentType().getName());
        formatoPEW4CE00.setNumidcy(dtoIntCustomer.getPartnerInformation().getDocumentNumber());
        formatoPEW4CE00.setNomidcy(dtoIntCustomer.getPartnerInformation().getName());

        StringBuilder naciona = new StringBuilder();
        naciona.append(dtoIntCustomer.getNationalities().get(0).getId());
        naciona.append(dtoIntCustomer.getNationalities().get(0).getName());
        formatoPEW4CE00.setNaciona(naciona.toString());

        formatoPEW4CE00.setFalleci(dtoIntCustomer.getHasPassesAway());

        formatoPEW4CE00.setIdtipdo(dtoIntCustomer.getIdentityDocument().getDocumentType().getId());
        formatoPEW4CE00.setNomtidc(dtoIntCustomer.getIdentityDocument().getDocumentType().getName());
        formatoPEW4CE00.setNumidcl(dtoIntCustomer.getIdentityDocument().getDocumentNumber());
        formatoPEW4CE00.setFeexped(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpeditionDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW4CE00.setFevenci(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpirationDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW4CE00.setIdpaiex(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getId());
        formatoPEW4CE00.setPaisexp(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getName());
        formatoPEW4CE00.setDptoexp(dtoIntCustomer.getIdentityDocument().getStateExpedition());
        formatoPEW4CE00.setCiudexp(dtoIntCustomer.getIdentityDocument().getCityExpedition());

        formatoPEW4CE00.setNumdepe(dtoIntCustomer.getNumberPersonAssociated());

        formatoPEW4CE00.setTipdire(dtoIntCustomer.getLegalAddresses().getType());
        formatoPEW4CE00.setIdpaidi(dtoIntCustomer.getLegalAddresses().getCountry().getId());
        formatoPEW4CE00.setPaisdir(dtoIntCustomer.getLegalAddresses().getCountry().getName());
        formatoPEW4CE00.setDptodir(dtoIntCustomer.getLegalAddresses().getRegion());
        formatoPEW4CE00.setCiuddir(dtoIntCustomer.getLegalAddresses().getCity());
        formatoPEW4CE00.setDirecci(dtoIntCustomer.getLegalAddresses().getAddressName());
        formatoPEW4CE00.setCodpost(dtoIntCustomer.getLegalAddresses().getZipCode());

        StringBuilder idConta = new StringBuilder();
        StringBuilder idTipco = new StringBuilder();
        StringBuilder nmTipco = new StringBuilder();
        StringBuilder isprefe = new StringBuilder();
        StringBuilder isCheck = new StringBuilder();
        for (DTOIntContactDetail dtoIntContactDetail:dtoIntCustomer.getContactDetail()) {
            idConta.append(dtoIntContactDetail.getId()).toString();
            idTipco.append(dtoIntContactDetail.getContactType().getId()).toString();
            nmTipco.append(dtoIntContactDetail.getContactType().getName()).toString();
            isprefe.append(dtoIntContactDetail.getIsPreferential().equals(true)?"S":"N");
            isCheck.append(dtoIntContactDetail.getIsChecked().equals(true)?"S":"N");
        }
        formatoPEW4CE00.setIdconta(idConta.toString());
        formatoPEW4CE00.setIdtipco(idTipco.toString());
        formatoPEW4CE00.setNmtipco(nmTipco.toString());
        formatoPEW4CE00.setIsprefe(isprefe.toString());
        formatoPEW4CE00.setIscheck(isCheck.toString());

        formatoPEW4CE00.setVlrco01(dtoIntCustomer.getContactDetail().get(0).getContact());
        formatoPEW4CE00.setVlrco02(dtoIntCustomer.getContactDetail().get(1).getContact());
        formatoPEW4CE00.setVlrco03(dtoIntCustomer.getContactDetail().get(2).getContact());
        formatoPEW4CE00.setVlrco04(dtoIntCustomer.getContactDetail().get(3).getContact());
        formatoPEW4CE00.setVlrco05(dtoIntCustomer.getContactDetail().get(4).getContact());
        formatoPEW4CE00.setVlrco06(dtoIntCustomer.getContactDetail().get(5).getContact());
        formatoPEW4CE00.setVlrco07(dtoIntCustomer.getContactDetail().get(6).getContact());
        formatoPEW4CE00.setVlrco08(dtoIntCustomer.getContactDetail().get(7).getContact());
        formatoPEW4CE00.setVlrco09(dtoIntCustomer.getContactDetail().get(8).getContact());
        formatoPEW4CE00.setVlrco10(dtoIntCustomer.getContactDetail().get(9).getContact());

        formatoPEW4CE00.setIugesto(dtoIntCustomer.getBankingRelationship().getManager());
        formatoPEW4CE00.setPeyclie(dtoIntCustomer.getBankingRelationship().getCustomerFlag());
        formatoPEW4CE00.setMarcvip(dtoIntCustomer.getBankingRelationship().getPremiumFlag());
        formatoPEW4CE00.setMarcdeb(dtoIntCustomer.getBankingRelationship().getDebitFlag());

        formatoPEW4CE00.setIdnives(dtoIntCustomer.getLevelStudy().getId());
        formatoPEW4CE00.setNiveest(dtoIntCustomer.getLevelStudy().getTitle());
        formatoPEW4CE00.setIdpaini(dtoIntCustomer.getLevelStudy().getCountry().getId());
        formatoPEW4CE00.setPaisnac(dtoIntCustomer.getLevelStudy().getCountry().getName());
        formatoPEW4CE00.setDptonac(dtoIntCustomer.getLevelStudy().getState());
        formatoPEW4CE00.setCiudnac(dtoIntCustomer.getLevelStudy().getCity());

        formatoPEW4CE00.setIdtipvi(dtoIntCustomer.getLivingPlace().getLivingtype().getId());
        formatoPEW4CE00.setNomtipv(dtoIntCustomer.getLivingPlace().getLivingtype().getName());
        formatoPEW4CE00.setIdrelvi(dtoIntCustomer.getLivingPlace().getRelationType().getId());
        formatoPEW4CE00.setNomrelv(dtoIntCustomer.getLivingPlace().getRelationType().getName());
        formatoPEW4CE00.setEstrato(dtoIntCustomer.getLivingPlace().getLivingLevel().toString());

        formatoPEW4CE00.setTitulo(dtoIntCustomer.getEmploymentInformation().getJobTittle());
        formatoPEW4CE00.setSociemp(dtoIntCustomer.getEmploymentInformation().getIsHolder());
        formatoPEW4CE00.setCargemp(dtoIntCustomer.getEmploymentInformation().getTypeEmployee());
        formatoPEW4CE00.setFeiniem(new DateConverter().toDateFormat(dtoIntCustomer.getEmploymentInformation().getStartDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW4CE00.setAntigue(dtoIntCustomer.getEmploymentInformation().getYearsAntiguaty().toString());
        formatoPEW4CE00.setIdnomem(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getId());
        formatoPEW4CE00.setNombemp(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getName());
        formatoPEW4CE00.setCiiu(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getCode());

        formatoPEW4CE00.setIndpep(dtoIntCustomer.getIsPublicPerson());

        return formatoPEW4CE00;


    }

    @Override
    public String mapOutFormatoPEW4CS00CreateCustomer(FormatoPEW4CS00 formatoPEW4CS00) {
        return formatoPEW4CS00.getNumclie();
    }

    @Override
    public FormatoPEW3CE00 mapInFortmatoPEW3CE00ModifyCustomer(DTOIntCustomerV2 dtoIntCustomer) {
        final FormatoPEW3CE00 formatoPEW3CE00 = new FormatoPEW3CE00();
        formatoPEW3CE00.setIdclien(dtoIntCustomer.getCustomerId());
        formatoPEW3CE00.setNombre(dtoIntCustomer.getFirstName());
        formatoPEW3CE00.setApellid(dtoIntCustomer.getLastName());
        formatoPEW3CE00.setFenacim(new DateConverter().toDateFormat(dtoIntCustomer.getBirthDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW3CE00.setEstaciv(dtoIntCustomer.getMaritalStatus());
        formatoPEW3CE00.setSexo(dtoIntCustomer.getGender());

        formatoPEW3CE00.setTipidcy(dtoIntCustomer.getPartnerInformation().getDocumentType().getId());
        formatoPEW3CE00.setNomtidy(dtoIntCustomer.getPartnerInformation().getDocumentType().getName());
        formatoPEW3CE00.setNumidcy(dtoIntCustomer.getPartnerInformation().getDocumentNumber());
        formatoPEW3CE00.setNomidcy(dtoIntCustomer.getPartnerInformation().getName());

        StringBuilder naciona = new StringBuilder();
        naciona.append(dtoIntCustomer.getNationalities().get(0).getId());
        naciona.append(dtoIntCustomer.getNationalities().get(0).getName());
        formatoPEW3CE00.setNaciona(naciona.toString());

        formatoPEW3CE00.setFalleci(dtoIntCustomer.getHasPassesAway());

        formatoPEW3CE00.setIdtipdo(dtoIntCustomer.getIdentityDocument().getDocumentType().getId());
        formatoPEW3CE00.setNomtidc(dtoIntCustomer.getIdentityDocument().getDocumentType().getName());
        formatoPEW3CE00.setNumidcl(dtoIntCustomer.getIdentityDocument().getDocumentNumber());
        formatoPEW3CE00.setFeexped(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpeditionDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW3CE00.setFevenci(new DateConverter().toDateFormat(dtoIntCustomer.getIdentityDocument().getExpirationDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW3CE00.setIdpaiex(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getId());
        formatoPEW3CE00.setPaisexp(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getName());
        formatoPEW3CE00.setDptoexp(dtoIntCustomer.getIdentityDocument().getStateExpedition());
        formatoPEW3CE00.setCiudexp(dtoIntCustomer.getIdentityDocument().getCityExpedition());

        formatoPEW3CE00.setNumdepe(dtoIntCustomer.getNumberPersonAssociated());

        formatoPEW3CE00.setTipdire(dtoIntCustomer.getLegalAddresses().getType());
        formatoPEW3CE00.setIdpaidi(dtoIntCustomer.getLegalAddresses().getCountry().getId());
        formatoPEW3CE00.setPaisdir(dtoIntCustomer.getLegalAddresses().getCountry().getName());
        formatoPEW3CE00.setDptodir(dtoIntCustomer.getLegalAddresses().getRegion());
        formatoPEW3CE00.setCiuddir(dtoIntCustomer.getLegalAddresses().getCity());
        formatoPEW3CE00.setDirecci(dtoIntCustomer.getLegalAddresses().getAddressName());
        formatoPEW3CE00.setCodpost(dtoIntCustomer.getLegalAddresses().getZipCode());

        StringBuilder idConta = new StringBuilder();
        StringBuilder idTipco = new StringBuilder();
        StringBuilder nmTipco = new StringBuilder();
        StringBuilder isprefe = new StringBuilder();
        StringBuilder isCheck = new StringBuilder();
        for (DTOIntContactDetail dtoIntContactDetail:dtoIntCustomer.getContactDetail()) {
            idConta.append(dtoIntContactDetail.getId());
            idTipco.append(dtoIntContactDetail.getContactType().getId());
            nmTipco.append(dtoIntContactDetail.getContactType().getName());
            isprefe.append(dtoIntContactDetail.getIsPreferential().equals(true)?"S":"N");
            isCheck.append(dtoIntContactDetail.getIsChecked().equals(true)?"S":"N");
        }
        formatoPEW3CE00.setIdconta(idConta.toString());
        formatoPEW3CE00.setIdtipco(idTipco.toString());
        formatoPEW3CE00.setNmtipco(nmTipco.toString());
        formatoPEW3CE00.setIsprefe(isprefe.toString());
        formatoPEW3CE00.setIscheck(isCheck.toString());

        formatoPEW3CE00.setVlrco01(dtoIntCustomer.getContactDetail().get(0).getContact());
        formatoPEW3CE00.setVlrco02(dtoIntCustomer.getContactDetail().get(1).getContact());
        formatoPEW3CE00.setVlrco03(dtoIntCustomer.getContactDetail().get(2).getContact());
        formatoPEW3CE00.setVlrco04(dtoIntCustomer.getContactDetail().get(3).getContact());
        formatoPEW3CE00.setVlrco05(dtoIntCustomer.getContactDetail().get(4).getContact());
        formatoPEW3CE00.setVlrco06(dtoIntCustomer.getContactDetail().get(5).getContact());
        formatoPEW3CE00.setVlrco07(dtoIntCustomer.getContactDetail().get(6).getContact());
        formatoPEW3CE00.setVlrco08(dtoIntCustomer.getContactDetail().get(7).getContact());
        formatoPEW3CE00.setVlrco09(dtoIntCustomer.getContactDetail().get(8).getContact());
        formatoPEW3CE00.setVlrco10(dtoIntCustomer.getContactDetail().get(9).getContact());

        formatoPEW3CE00.setIugesto(dtoIntCustomer.getBankingRelationship().getManager());
        formatoPEW3CE00.setPeyclie(dtoIntCustomer.getBankingRelationship().getCustomerFlag());
        formatoPEW3CE00.setMarcvip(dtoIntCustomer.getBankingRelationship().getPremiumFlag());
        formatoPEW3CE00.setMarcdeb(dtoIntCustomer.getBankingRelationship().getDebitFlag());

        formatoPEW3CE00.setIdnives(dtoIntCustomer.getLevelStudy().getId());
        formatoPEW3CE00.setNiveest(dtoIntCustomer.getLevelStudy().getTitle());
        formatoPEW3CE00.setIdpaini(dtoIntCustomer.getLevelStudy().getCountry().getId());
        formatoPEW3CE00.setPaisnac(dtoIntCustomer.getLevelStudy().getCountry().getName());
        formatoPEW3CE00.setDptonac(dtoIntCustomer.getLevelStudy().getState());
        formatoPEW3CE00.setCiudnac(dtoIntCustomer.getLevelStudy().getCity());

        formatoPEW3CE00.setIdtipvi(dtoIntCustomer.getLivingPlace().getLivingtype().getId());
        formatoPEW3CE00.setNomtipv(dtoIntCustomer.getLivingPlace().getLivingtype().getName());
        formatoPEW3CE00.setIdrelvi(dtoIntCustomer.getLivingPlace().getRelationType().getId());
        formatoPEW3CE00.setNomrelv(dtoIntCustomer.getLivingPlace().getRelationType().getName());
        formatoPEW3CE00.setEstrato(dtoIntCustomer.getLivingPlace().getLivingLevel().toString());

        formatoPEW3CE00.setTitulo(dtoIntCustomer.getEmploymentInformation().getJobTittle());
        formatoPEW3CE00.setSociemp(dtoIntCustomer.getEmploymentInformation().getIsHolder());
        formatoPEW3CE00.setCargemp(dtoIntCustomer.getEmploymentInformation().getTypeEmployee());
        formatoPEW3CE00.setFeiniem(new DateConverter().toDateFormat(dtoIntCustomer.getEmploymentInformation().getStartDate(), DateFormatEnum.YYYY_MM_DD_TIME_FORMAT));
        formatoPEW3CE00.setAntigue(dtoIntCustomer.getEmploymentInformation().getYearsAntiguaty().toString());
        formatoPEW3CE00.setIdnomem(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getId());
        formatoPEW3CE00.setNombemp(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getName());
        formatoPEW3CE00.setCiiu(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getCode());

        formatoPEW3CE00.setIndpep(dtoIntCustomer.getIsPublicPerson());

        return formatoPEW3CE00;
    }
}
