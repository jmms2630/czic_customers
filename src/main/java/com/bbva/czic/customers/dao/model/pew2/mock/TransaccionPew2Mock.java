package com.bbva.czic.customers.dao.model.pew2.mock;

import com.bbva.czic.customers.dao.model.pew2.*;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Invocador de la transacci&oacute;n <code>PEW2</code>
 * 
 * @see com.bbva.czic.customers.dao.model.pew2.PeticionTransaccionPew2
 * @see com.bbva.czic.customers.dao.model.pew2.RespuestaTransaccionPew2
 */

@Component(value = "transaccionPew2")
@Profile(value = "dev")
public class TransaccionPew2Mock implements InvocadorTransaccion<PeticionTransaccionPew2,RespuestaTransaccionPew2> {

	@Override
	public RespuestaTransaccionPew2 invocar(final PeticionTransaccionPew2 transaccion) throws ExcepcionTransaccion {

		final RespuestaTransaccionPew2 respuestaTransaccionPew0 = new RespuestaTransaccionPew2();

		final FormatoPEW2CE00 formatoPEW2CE00 = (FormatoPEW2CE00)transaccion.getCuerpo().getPartes().get(0);

		final Integer value = formatoPEW2CE00.getIdclien() != null ? Integer.parseInt(formatoPEW2CE00.getIdclien()) : 70;

		respuestaTransaccionPew0.getCuerpo().getPartes().addAll(getAllFormatoPEW2CS(value));

		return respuestaTransaccionPew0;
	}

	@Override
	public RespuestaTransaccionPew2 invocarCache(final PeticionTransaccionPew2 transaccion) throws ExcepcionTransaccion {
		return null;
	}

	@Override
	public void vaciarCache() {
	}

	private List<CopySalida> getAllFormatoPEW2CS(Integer id) {

		final List<CopySalida> copies = new ArrayList<CopySalida>();

		List<FormatoPEW2CS00> formatoPEW0CS00;
		List<FormatoPEW2CS01> formatoPEW0CS01;
		List<FormatoPEW2CS02> formatoPEW0CS02;
		List<FormatoPEW2CS03> formatoPEW0CS03;
		List<FormatoPEW2CS04> formatoPEW0CS04;
		List<FormatoPEW2CS05> formatoPEW0CS05;


		String ruta = null,ruta1 = null, ruta2 = null, ruta3 = null,ruta4 = null, ruta5 = null;
switch (id){
	case 0:
		ruta = "mocks/FormatoPEW2CS00_Ok.json";
		ruta1 = "mocks/FormatoPEW2CS01_Ok.json";
		ruta2 = "mocks/FormatoPEW2CS02_Ok.json";
		ruta3 = "mocks/FormatoPEW2CS03_Ok.json";
		ruta4 = "mocks/FormatoPEW2CS04_Ok.json";
		ruta5 = "mocks/FormatoPEW2CS05_Ok.json";
		break;
	case 10:
		ruta = "mocks/FormatoPEW2CS00_Ok.json";
		ruta1 = "mocks/FormatoPEW2CS01_Ok.json";
		ruta2 = "mocks/FormatoPEW2CS02_Ok.json";
		ruta3 = "mocks/FormatoPEW2CS03_NacionalidadNull.json";
		ruta4 = "mocks/FormatoPEW2CS04_Ok.json";
		ruta5 = "mocks/FormatoPEW2CS05_Ok.json";
	break;

	case 20:
		ruta = "mocks/FormatoPEW2CS00_Ok.json";
		ruta1 = "mocks/FormatoPEW2CS01_ContactsDetailNull.json";
		ruta2 = "mocks/FormatoPEW2CS02_Ok.json";
		ruta3 = "mocks/FormatoPEW2CS03_Ok.json";
		ruta4 = "mocks/FormatoPEW2CS04_Ok.json";
		ruta5 = "mocks/FormatoPEW2CS05_Ok.json";
	break;

	case 30:
		ruta = "mocks/FormatoPEW2CS00_BirtdateNull.json";
		ruta1 = "mocks/FormatoPEW2CS01_Ok.json";
		ruta2 = "mocks/FormatoPEW2CS02_Ok.json";
		ruta3 = "mocks/FormatoPEW2CS03_Ok.json";
		ruta4 = "mocks/FormatoPEW2CS04_Ok.json";
		ruta5 = "mocks/FormatoPEW2CS05_Ok.json";
		break;

	default:
		ruta = "mocks/FormatoPEW2CS00_Ok.json";
		ruta1 = "mocks/FormatoPEW2CS01_Ok.json";
		ruta2 = "mocks/FormatoPEW2CS02_Ok.json";
		ruta3 = "mocks/FormatoPEW2CS03_Ok.json";
		ruta4 = "mocks/FormatoPEW2CS04_Ok.json";
		ruta5 = "mocks/FormatoPEW2CS05_Ok.json";
	break;
}


		try {
			formatoPEW0CS00 = new ObjectMapper().readValue(this.getClass().getClassLoader().getResourceAsStream(ruta),
					new TypeReference<List<FormatoPEW2CS00>>() {
					});

			formatoPEW0CS01 = new ObjectMapper().readValue(this.getClass().getClassLoader().getResourceAsStream(ruta1),
					new TypeReference<List<FormatoPEW2CS01>>() {
					});

			formatoPEW0CS02 = new ObjectMapper().readValue(this.getClass().getClassLoader().getResourceAsStream(ruta2),
					new TypeReference<List<FormatoPEW2CS02>>() {
					});

			formatoPEW0CS03 = new ObjectMapper().readValue(this.getClass().getClassLoader().getResourceAsStream(ruta3),
					new TypeReference<List<FormatoPEW2CS03>>() {
					});

			formatoPEW0CS04 = new ObjectMapper().readValue(this.getClass().getClassLoader().getResourceAsStream(ruta4),
					new TypeReference<List<FormatoPEW2CS04>>() {
					});

			formatoPEW0CS05 = new ObjectMapper().readValue(this.getClass().getClassLoader().getResourceAsStream(ruta5),
					new TypeReference<List<FormatoPEW2CS05>>() {
					});

			for (final FormatoPEW2CS00 item : formatoPEW0CS00) {
				CopySalida copy = new CopySalida();
				copy.setCopy(item);
				copies.add(copy);
			}

			for (final FormatoPEW2CS01 item : formatoPEW0CS01) {
				CopySalida copy = new CopySalida();
				copy.setCopy(item);
				copies.add(copy);
			}

			for (final FormatoPEW2CS02 item : formatoPEW0CS02) {
				CopySalida copy = new CopySalida();
				copy.setCopy(item);
				copies.add(copy);
			}

			for (final FormatoPEW2CS03 item : formatoPEW0CS03) {
				CopySalida copy = new CopySalida();
				copy.setCopy(item);
				copies.add(copy);
			}

			for (final FormatoPEW2CS04 item : formatoPEW0CS04) {
				CopySalida copy = new CopySalida();
				copy.setCopy(item);
				copies.add(copy);
			}

			for (final FormatoPEW2CS05 item : formatoPEW0CS05) {
				CopySalida copy = new CopySalida();
				copy.setCopy(item);
				copies.add(copy);
			}



		} catch (final IOException e) {
			e.printStackTrace();
		}
		return copies;
	}

}