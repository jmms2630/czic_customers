package com.bbva.czic.customers.dao.tx;

import com.bbva.ccol.commons.rm.utils.tx.impl.SimpleNoResultBbvaTransaction;
import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.dao.mappers.ITxCustomrMapper;
import com.bbva.czic.customers.dao.model.pew3.FormatoPEW3CE00;
import com.bbva.czic.customers.dao.model.pew3.PeticionTransaccionPew3;
import com.bbva.czic.customers.dao.model.pew3.RespuestaTransaccionPew3;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Entelgy on 25/08/2016.
 */
@Component(value = "txModifyCustomer")
public class TxModifyCustomer extends SimpleNoResultBbvaTransaction<DTOIntCustomerV2, FormatoPEW3CE00, Void> {
    @Resource(name="transaccionPew3")
    private transient InvocadorTransaccion<PeticionTransaccionPew3, RespuestaTransaccionPew3> transaccionPew3;

    @Resource(name = "txCustomrMapper")
    private ITxCustomrMapper txCustomrMapper;

    @Override
    protected FormatoPEW3CE00 mapDtoInToRequestFormat(DTOIntCustomerV2 dtoIntCustomerV2) {
        return txCustomrMapper.mapInFortmatoPEW3CE00ModifyCustomer(dtoIntCustomerV2);
    }

    @Override
    protected InvocadorTransaccion<?, ?> getTransaction() {
        return transaccionPew3;
    }

}
