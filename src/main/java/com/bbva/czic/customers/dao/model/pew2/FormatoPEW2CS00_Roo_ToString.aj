// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.czic.customers.dao.model.pew2;

import java.lang.String;

privileged aspect FormatoPEW2CS00_Roo_ToString {
    
    public String FormatoPEW2CS00.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Antigue: ").append(getAntigue()).append(", ");
        sb.append("Apellid: ").append(getApellid()).append(", ");
        sb.append("Cargemp: ").append(getCargemp()).append(", ");
        sb.append("Ciiu: ").append(getCiiu()).append(", ");
        sb.append("Ciudnac: ").append(getCiudnac()).append(", ");
        sb.append("Dptonac: ").append(getDptonac()).append(", ");
        sb.append("Estaciv: ").append(getEstaciv()).append(", ");
        sb.append("Estrato: ").append(getEstrato()).append(", ");
        sb.append("Falleci: ").append(getFalleci()).append(", ");
        sb.append("Feiniem: ").append(getFeiniem()).append(", ");
        sb.append("Fenacim: ").append(getFenacim()).append(", ");
        sb.append("Idclien: ").append(getIdclien()).append(", ");
        sb.append("Idnives: ").append(getIdnives()).append(", ");
        sb.append("Idnomem: ").append(getIdnomem()).append(", ");
        sb.append("Idpaini: ").append(getIdpaini()).append(", ");
        sb.append("Idrelvi: ").append(getIdrelvi()).append(", ");
        sb.append("Idtipvi: ").append(getIdtipvi()).append(", ");
        sb.append("Indpep: ").append(getIndpep()).append(", ");
        sb.append("Iugesto: ").append(getIugesto()).append(", ");
        sb.append("Marcdeb: ").append(getMarcdeb()).append(", ");
        sb.append("Marcvip: ").append(getMarcvip()).append(", ");
        sb.append("Niveest: ").append(getNiveest()).append(", ");
        sb.append("Nombemp: ").append(getNombemp()).append(", ");
        sb.append("Nombre: ").append(getNombre()).append(", ");
        sb.append("Nomrelv: ").append(getNomrelv()).append(", ");
        sb.append("Nomtipv: ").append(getNomtipv()).append(", ");
        sb.append("Numdepe: ").append(getNumdepe()).append(", ");
        sb.append("Paisnac: ").append(getPaisnac()).append(", ");
        sb.append("Peyclie: ").append(getPeyclie()).append(", ");
        sb.append("Sexo: ").append(getSexo()).append(", ");
        sb.append("Sociemp: ").append(getSociemp()).append(", ");
        sb.append("Titulo: ").append(getTitulo());
        return sb.toString();
    }
    
}
