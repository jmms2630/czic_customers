package com.bbva.czic.customers.dao.model.pew2;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>PEW2CE00</code> de la transacci&oacute;n <code>PEW2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEW2CE00")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEW2CE00 {

	/**
	 * <p>Campo <code>IDCLIEN</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDCLIEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String idclien;
	
}