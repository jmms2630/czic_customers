package com.bbva.czic.customers.dao.model.pew2;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>PEW2CS02</code> de la transacci&oacute;n <code>PEW2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEW2CS02")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEW2CS02 {
	
	/**
	 * <p>Campo <code>TIPIDCY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPIDCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipidcy;
	
	/**
	 * <p>Campo <code>NOMTIDY</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NOMTIDY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String nomtidy;
	
	/**
	 * <p>Campo <code>NUMIDCY</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NUMIDCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String numidcy;
	
	/**
	 * <p>Campo <code>NOMIDCY</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "NOMIDCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String nomidcy;
	
}