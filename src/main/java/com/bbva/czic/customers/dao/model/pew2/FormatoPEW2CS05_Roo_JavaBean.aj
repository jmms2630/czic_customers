// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.czic.customers.dao.model.pew2;

import java.lang.String;

privileged aspect FormatoPEW2CS05_Roo_JavaBean {
    
    public String FormatoPEW2CS05.getTipdire() {
        return this.tipdire;
    }
    
    public void FormatoPEW2CS05.setTipdire(String tipdire) {
        this.tipdire = tipdire;
    }
    
    public String FormatoPEW2CS05.getIdpaidi() {
        return this.idpaidi;
    }
    
    public void FormatoPEW2CS05.setIdpaidi(String idpaidi) {
        this.idpaidi = idpaidi;
    }
    
    public String FormatoPEW2CS05.getPaisdir() {
        return this.paisdir;
    }
    
    public void FormatoPEW2CS05.setPaisdir(String paisdir) {
        this.paisdir = paisdir;
    }
    
    public String FormatoPEW2CS05.getDptodir() {
        return this.dptodir;
    }
    
    public void FormatoPEW2CS05.setDptodir(String dptodir) {
        this.dptodir = dptodir;
    }
    
    public String FormatoPEW2CS05.getCiuddir() {
        return this.ciuddir;
    }
    
    public void FormatoPEW2CS05.setCiuddir(String ciuddir) {
        this.ciuddir = ciuddir;
    }
    
    public String FormatoPEW2CS05.getDirecci() {
        return this.direcci;
    }
    
    public void FormatoPEW2CS05.setDirecci(String direcci) {
        this.direcci = direcci;
    }
    
    public String FormatoPEW2CS05.getCodpost() {
        return this.codpost;
    }
    
    public void FormatoPEW2CS05.setCodpost(String codpost) {
        this.codpost = codpost;
    }
    
}
