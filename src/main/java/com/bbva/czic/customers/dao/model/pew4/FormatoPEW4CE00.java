package com.bbva.czic.customers.dao.model.pew4;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>PEW4CE00</code> de la transacci&oacute;n <code>PEW4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEW4CE00")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEW4CE00 {

	/**
	 * <p>Campo <code>IDCLIEN</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDCLIEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String idclien;
	
	/**
	 * <p>Campo <code>NOMBRE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NOMBRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nombre;
	
	/**
	 * <p>Campo <code>APELLID</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "APELLID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String apellid;
	
	/**
	 * <p>Campo <code>FENACIM</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "FENACIM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String fenacim;
	
	/**
	 * <p>Campo <code>ESTACIV</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "ESTACIV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String estaciv;
	
	/**
	 * <p>Campo <code>SEXO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "SEXO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String sexo;
	
	/**
	 * <p>Campo <code>TIPIDCY</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "TIPIDCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipidcy;
	
	/**
	 * <p>Campo <code>NOMTIDY</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "NOMTIDY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String nomtidy;
	
	/**
	 * <p>Campo <code>NUMIDCY</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "NUMIDCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String numidcy;
	
	/**
	 * <p>Campo <code>NOMIDCY</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "NOMIDCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String nomidcy;
	
	/**
	 * <p>Campo <code>NACIONA</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "NACIONA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String naciona;
	
	/**
	 * <p>Campo <code>FALLECI</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "FALLECI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String falleci;
	
	/**
	 * <p>Campo <code>IDTIPDO</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "IDTIPDO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtipdo;
	
	/**
	 * <p>Campo <code>NOMTIDC</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "NOMTIDC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomtidc;
	
	/**
	 * <p>Campo <code>NUMIDCL</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "NUMIDCL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String numidcl;
	
	/**
	 * <p>Campo <code>FEEXPED</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "FEEXPED", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String feexped;
	
	/**
	 * <p>Campo <code>FEVENCI</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "FEVENCI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String fevenci;
	
	/**
	 * <p>Campo <code>IDPAIEX</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "IDPAIEX", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idpaiex;
	
	/**
	 * <p>Campo <code>PAISEXP</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "PAISEXP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String paisexp;
	
	/**
	 * <p>Campo <code>DPTOEXP</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "DPTOEXP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String dptoexp;
	
	/**
	 * <p>Campo <code>CIUDEXP</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "CIUDEXP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ciudexp;
	
	/**
	 * <p>Campo <code>NUMDEPE</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "NUMDEPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String numdepe;
	
	/**
	 * <p>Campo <code>TIPDIRE</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "TIPDIRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String tipdire;
	
	/**
	 * <p>Campo <code>IDPAIDI</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "IDPAIDI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idpaidi;
	
	/**
	 * <p>Campo <code>PAISDIR</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "PAISDIR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String paisdir;
	
	/**
	 * <p>Campo <code>DPTODIR</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "DPTODIR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String dptodir;
	
	/**
	 * <p>Campo <code>CIUDDIR</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "CIUDDIR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ciuddir;
	
	/**
	 * <p>Campo <code>DIRECCI</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "DIRECCI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String direcci;
	
	/**
	 * <p>Campo <code>CODPOST</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 29, nombre = "CODPOST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String codpost;
	
	/**
	 * <p>Campo <code>IDCONTA</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "IDCONTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String idconta;
	
	/**
	 * <p>Campo <code>VLRCO01</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 31, nombre = "VLRCO01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco01;
	
	/**
	 * <p>Campo <code>VLRCO02</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "VLRCO02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco02;
	
	/**
	 * <p>Campo <code>VLRCO03</code>, &iacute;ndice: <code>33</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 33, nombre = "VLRCO03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco03;
	
	/**
	 * <p>Campo <code>VLRCO04</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 34, nombre = "VLRCO04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco04;
	
	/**
	 * <p>Campo <code>VLRCO05</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 35, nombre = "VLRCO05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco05;
	
	/**
	 * <p>Campo <code>VLRCO06</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 36, nombre = "VLRCO06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco06;
	
	/**
	 * <p>Campo <code>VLRCO07</code>, &iacute;ndice: <code>37</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 37, nombre = "VLRCO07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco07;
	
	/**
	 * <p>Campo <code>VLRCO08</code>, &iacute;ndice: <code>38</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 38, nombre = "VLRCO08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco08;
	
	/**
	 * <p>Campo <code>VLRCO09</code>, &iacute;ndice: <code>39</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 39, nombre = "VLRCO09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco09;
	
	/**
	 * <p>Campo <code>VLRCO10</code>, &iacute;ndice: <code>40</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 40, nombre = "VLRCO10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String vlrco10;
	
	/**
	 * <p>Campo <code>IDTIPCO</code>, &iacute;ndice: <code>41</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 41, nombre = "IDTIPCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 100, longitudMaxima = 100)
	private String idtipco;
	
	/**
	 * <p>Campo <code>NMTIPCO</code>, &iacute;ndice: <code>42</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 42, nombre = "NMTIPCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 100, longitudMaxima = 100)
	private String nmtipco;
	
	/**
	 * <p>Campo <code>ISPREFE</code>, &iacute;ndice: <code>43</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 43, nombre = "ISPREFE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String isprefe;
	
	/**
	 * <p>Campo <code>ISCHECK</code>, &iacute;ndice: <code>44</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 44, nombre = "ISCHECK", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ischeck;
	
	/**
	 * <p>Campo <code>IUGESTO</code>, &iacute;ndice: <code>45</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 45, nombre = "IUGESTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String iugesto;
	
	/**
	 * <p>Campo <code>PEYCLIE</code>, &iacute;ndice: <code>46</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 46, nombre = "PEYCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String peyclie;
	
	/**
	 * <p>Campo <code>MARCVIP</code>, &iacute;ndice: <code>47</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 47, nombre = "MARCVIP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String marcvip;
	
	/**
	 * <p>Campo <code>MARCDEB</code>, &iacute;ndice: <code>48</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 48, nombre = "MARCDEB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String marcdeb;
	
	/**
	 * <p>Campo <code>IDNIVES</code>, &iacute;ndice: <code>49</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 49, nombre = "IDNIVES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idnives;
	
	/**
	 * <p>Campo <code>NIVEEST</code>, &iacute;ndice: <code>50</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 50, nombre = "NIVEEST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String niveest;
	
	/**
	 * <p>Campo <code>IDPAINI</code>, &iacute;ndice: <code>51</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 51, nombre = "IDPAINI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idpaini;
	
	/**
	 * <p>Campo <code>PAISNAC</code>, &iacute;ndice: <code>52</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 52, nombre = "PAISNAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String paisnac;
	
	/**
	 * <p>Campo <code>DPTONAC</code>, &iacute;ndice: <code>53</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 53, nombre = "DPTONAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String dptonac;
	
	/**
	 * <p>Campo <code>CIUDNAC</code>, &iacute;ndice: <code>54</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 54, nombre = "CIUDNAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ciudnac;
	
	/**
	 * <p>Campo <code>IDTIPVI</code>, &iacute;ndice: <code>55</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 55, nombre = "IDTIPVI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idtipvi;
	
	/**
	 * <p>Campo <code>NOMTIPV</code>, &iacute;ndice: <code>56</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 56, nombre = "NOMTIPV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String nomtipv;
	
	/**
	 * <p>Campo <code>IDRELVI</code>, &iacute;ndice: <code>57</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 57, nombre = "IDRELVI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idrelvi;
	
	/**
	 * <p>Campo <code>NOMRELV</code>, &iacute;ndice: <code>58</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 58, nombre = "NOMRELV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomrelv;
	
	/**
	 * <p>Campo <code>ESTRATO</code>, &iacute;ndice: <code>59</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 59, nombre = "ESTRATO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String estrato;
	
	/**
	 * <p>Campo <code>TITULO</code>, &iacute;ndice: <code>60</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 60, nombre = "TITULO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String titulo;
	
	/**
	 * <p>Campo <code>SOCIEMP</code>, &iacute;ndice: <code>61</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 61, nombre = "SOCIEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String sociemp;
	
	/**
	 * <p>Campo <code>CARGEMP</code>, &iacute;ndice: <code>62</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 62, nombre = "CARGEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String cargemp;
	
	/**
	 * <p>Campo <code>FEINIEM</code>, &iacute;ndice: <code>63</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 63, nombre = "FEINIEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String feiniem;
	
	/**
	 * <p>Campo <code>ANTIGUE</code>, &iacute;ndice: <code>64</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 64, nombre = "ANTIGUE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String antigue;
	
	/**
	 * <p>Campo <code>IDNOMEM</code>, &iacute;ndice: <code>65</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 65, nombre = "IDNOMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idnomem;
	
	/**
	 * <p>Campo <code>NOMBEMP</code>, &iacute;ndice: <code>66</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 66, nombre = "NOMBEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String nombemp;
	
	/**
	 * <p>Campo <code>CIIU</code>, &iacute;ndice: <code>67</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 67, nombre = "CIIU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ciiu;
	
	/**
	 * <p>Campo <code>INDPEP</code>, &iacute;ndice: <code>68</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 68, nombre = "INDPEP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indpep;
	
}