package com.bbva.czic.customers.dao.model.pew4;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>PEW4</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPew4</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPew4</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: CCTPEW4.txt
 * PEW4ALTA CLIENTE EKIP - DATOS BASICOS  PE        PE1CPEW4BVDPEPO PEW4CE00            PEW4  NN3000CNNNNN    SSTN A      SNNSSNNN  NN                2016-08-04C343566 2016-08-0416.27.23CE15005 2016-08-04-15.32.04.244556C343566 0001-01-010001-01-01
 * FICHERO: FDFPEW4CE00.txt
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|01|00001|IDCLIEN|NUMERO ID DE CLIENTE|A|016|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|02|00017|NOMBRE |NOMBRE DE CLIENTE   |A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|03|00037|APELLID|APELLIDOS CLIENTE   |A|040|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|04|00077|FENACIM|FECHA DE NACIMIENTO |A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|05|00087|ESTACIV|ESTADO CIVIL        |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|06|00088|SEXO   |SEXO                |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|07|00089|TIPIDCY|TIPO ID CONYUGUE    |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|08|00090|NOMTIDY|NOMBRE TIPO ID CONYU|A|025|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|09|00115|NUMIDCY|IDENTIFICACION CNYGU|A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|10|00130|NOMIDCY|NOMBRE CONYUGUE     |A|060|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|11|00190|NACIONA|NACIONALIDADES      |A|016|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|12|00206|FALLECI|IND CLIENTE FALLECID|A|001|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|13|00207|IDTIPDO|ID TIPO DOCUM CLIENT|A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|14|00208|NOMTIDC|NOMBRE ID TIPO DOC C|A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|15|00228|NUMIDCL|NUMERO ID CLIENTE   |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|16|00243|FEEXPED|FECHA EXPEDICION ID |A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|17|00253|FEVENCI|FECHA VENCIMIENTO ID|A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|18|00263|IDPAIEX|ID PAIS EXPEDICION D|A|003|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|19|00266|PAISEXP|PAIS EXPEDICION ID  |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|20|00281|DPTOEXP|DEPARTAMENTO EXPEDIC|A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|21|00301|CIUDEXP|CIUDAD EXPEDICION   |A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|22|00321|NUMDEPE|NUMERO DEPENDIENTES |A|002|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|23|00323|TIPDIRE|TIPO DIRECCION      |A|003|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|24|00326|IDPAIDI|ID PAIS DIRECCION   |A|003|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|25|00329|PAISDIR|PAIS DIRECCION      |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|26|00344|DPTODIR|DEPARTAMENTO DIRECCI|A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|27|00364|CIUDDIR|CIUDAD DIRECCION    |A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|28|00384|DIRECCI|DIRECCION CLIENTE   |A|060|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|29|00444|CODPOST|CODIGO POSTAL       |A|005|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|30|00449|IDCONTA|ID CONTACTOS        |A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|31|00469|VLRCO01|VALOR CONTACTO 01   |A|080|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|32|00549|VLRCO02|VALOR CONTACTO 02   |A|080|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|33|00629|VLRCO03|VALOR CONTACTO 03   |A|080|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|34|00709|VLRCO04|VALOR CONTACTO 04   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|35|00789|VLRCO05|VALOR CONTACTO 05   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|36|00869|VLRCO06|VALOR CONTACTO 06   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|37|00949|VLRCO07|VALOR CONTACTO 07   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|38|01029|VLRCO08|VALOR CONTACTO 08   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|39|01109|VLRCO09|VALOR CONTACTO 09   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|40|01189|VLRCO10|VALOR CONTACTO 10   |A|080|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|41|01269|IDTIPCO|ID TIPOS DE CONTACTO|A|100|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|42|01369|NMTIPCO|NOMBRE DE TIPOS DE C|A|100|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|43|01469|ISPREFE|ARE PREFERED        |A|010|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|44|01479|ISCHECK|ARE CHECKED         |A|010|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|45|01489|IUGESTO|IUGESTOR            |A|007|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|46|01496|PEYCLIE|ESTADO CLIENTE      |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|47|01497|MARCVIP|MARCA CLIENTE VIP   |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|48|01498|MARCDEB|MARCDEB             |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|49|01499|IDNIVES|ID NIVEL DE ESTUDIOS|A|003|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|50|01502|NIVEEST|NIVEL DE ESTUDIOS   |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|51|01517|IDPAINI|ID PAIS NACIMIENTO  |A|003|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|52|01520|PAISNAC|PAIS DE NACIMIENTO  |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|53|01535|DPTONAC|DEPARTAMENTO NACIMIE|A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|54|01555|CIUDNAC|CIUDAD DE NACIMIENTO|A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|55|01575|IDTIPVI|ID TIPO DE VIVIENDA |A|003|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|56|01578|NOMTIPV|NOMBRE TIPO VIVIENDA|A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|57|01593|IDRELVI|ID RELACION VIVIENDA|A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|58|01603|NOMRELV|NOMBRE RELA VIVIENDA|A|020|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|59|01623|ESTRATO|ESTRATO CLIENTE     |A|001|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|60|01624|TITULO |TITULO PROFESIONAL  |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|61|01639|SOCIEMP|ES SOCIO DE EMPRESA |A|001|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|62|01640|CARGEMP|CARGO EN LA EMPRESA |A|015|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|63|01655|FEINIEM|FECHA INICIO EMPRESA|A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|64|01665|ANTIGUE|ANTIGUEDAD EN EMPRES|A|002|0|O|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|65|01667|IDNOMEM|ID NOMBRE EMPRESA   |A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|66|01677|NOMBEMP|NOMBRE DE LA EMPRESA|A|050|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|67|01727|CIIU   |CODIGO CIIU         |A|010|0|R|        |
 * PEW4CE00|E - E - ALTA CLIENTE EKIP     |F|68|01737|68|01737|INDPEP |INDICADOR PEP       |A|001|0|R|        |
 * FICHERO: FDFPEW4CS00.txt
 * PEW4CS00|S - S - ALTA CLIENTE EKIP - NU|X|01|00008|01|00001|NUMCLIE|NUMERO DE CLIENTE AL|A|008|0|S|        |
 * FICHERO: FDXPEW4.txt
 * PEW4PEW4CS00COPY    PE1CPEW41S                             CE15005 2016-08-04-16.30.06.258281CE15005 2016-08-04-16.30.06.258309
</pre></code>
 * 
 * @see RespuestaTransaccionPew4
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PEW4",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionPew4.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoPEW4CE00.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPew4 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}