package com.bbva.czic.customers.dao.model.pew2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PEW2</code>
 * 
 * @see PeticionTransaccionPew2
 * @see RespuestaTransaccionPew2
 */
@Component(value = "transaccionPew2")
@Profile(value = "prod")
public class TransaccionPew2 implements InvocadorTransaccion<PeticionTransaccionPew2,RespuestaTransaccionPew2> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPew2 invocar(PeticionTransaccionPew2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPew2.class, RespuestaTransaccionPew2.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPew2 invocarCache(PeticionTransaccionPew2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPew2.class, RespuestaTransaccionPew2.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}