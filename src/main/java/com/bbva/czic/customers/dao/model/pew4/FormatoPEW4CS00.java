package com.bbva.czic.customers.dao.model.pew4;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>PEW4CS00</code> de la transacci&oacute;n <code>PEW4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEW4CS00")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEW4CS00 {
	
	/**
	 * <p>Campo <code>NUMCLIE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String numclie;
	
}