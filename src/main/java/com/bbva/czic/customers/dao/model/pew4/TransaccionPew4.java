package com.bbva.czic.customers.dao.model.pew4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PEW4</code>
 * 
 * @see PeticionTransaccionPew4
 * @see RespuestaTransaccionPew4
 */
@Component(value = "transaccionPew4")
@Profile(value = "prod")
public class TransaccionPew4 implements InvocadorTransaccion<PeticionTransaccionPew4,RespuestaTransaccionPew4> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPew4 invocar(PeticionTransaccionPew4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPew4.class, RespuestaTransaccionPew4.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPew4 invocarCache(PeticionTransaccionPew4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPew4.class, RespuestaTransaccionPew4.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}