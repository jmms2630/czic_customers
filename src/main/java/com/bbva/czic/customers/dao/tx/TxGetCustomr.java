package com.bbva.czic.customers.dao.tx;

import com.bbva.ccol.commons.rm.utils.tx.impl.SextupleBbvaTransactionToObject;
import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.dao.mappers.ITxCustomrMapper;
import com.bbva.czic.customers.dao.model.pew2.*;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.zic.commons.rm.utils.tx.impl.SimpleBbvaTransaction;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Entelgy on 12/08/2016.
 */
@Component(value = "txGetCustomer")
public class TxGetCustomr extends SextupleBbvaTransactionToObject<String,FormatoPEW2CE00,DTOIntCustomerV2,FormatoPEW2CS00,FormatoPEW2CS01,FormatoPEW2CS02,FormatoPEW2CS03,FormatoPEW2CS04,FormatoPEW2CS05> {

    @Resource(name="transaccionPew2")
    private transient InvocadorTransaccion<PeticionTransaccionPew2, RespuestaTransaccionPew2> transaccionPew2;

    @Resource(name = "txCustomrMapper")
    private ITxCustomrMapper txCustomrMapper;

    @Override
    protected FormatoPEW2CE00 mapDtoInToRequestFormat(String dtoIn) {
        return txCustomrMapper.mapInFormatoPEW2CE00GetCustomer(dtoIn);
    }

    @Override
    protected DTOIntCustomerV2 mapResponseFormatAToDtoOut(FormatoPEW2CS00 formatOutput, String dtoIn, DTOIntCustomerV2 dtoOut) {
        return txCustomrMapper.mapOutFormatoPEW2CS00GetCustomer(formatOutput,dtoOut);
    }

    @Override
    protected DTOIntCustomerV2 mapResponseFormatBToDtoOut(FormatoPEW2CS01 formatOutput, String dtoIn, DTOIntCustomerV2 dtoOut) {
        return txCustomrMapper.mapOutFormatoPEW2CS01GetCustomer(formatOutput,dtoOut);
    }

    @Override
    protected DTOIntCustomerV2 mapResponseFormatCToDtoOut(FormatoPEW2CS02 formatOutput, String dtoIn, DTOIntCustomerV2 dtoOut) {
        return txCustomrMapper.mapOutFormatoPEW2CS02GetCustomer(formatOutput,dtoOut);
    }

    @Override
    protected DTOIntCustomerV2 mapResponseFormatDToDtoOut(FormatoPEW2CS03 formatOutput, String dtoIn, DTOIntCustomerV2 dtoOut) {
        return txCustomrMapper.mapOutFormatoPEW2CS03GetCustomer(formatOutput,dtoOut);
    }

    @Override
    protected DTOIntCustomerV2 mapResponseFormatEToDtoOut(FormatoPEW2CS04 formatOutput, String dtoIn, DTOIntCustomerV2 dtoOut) {
        return txCustomrMapper.mapOutFormatoPEW2CS04GetCustomer(formatOutput,dtoOut);
    }

    @Override
    protected DTOIntCustomerV2 mapResponseFormatFToDtoOut(FormatoPEW2CS05 formatOutput, String dtoIn, DTOIntCustomerV2 dtoOut) {
        return txCustomrMapper.mapOutFormatoPEW2CS05GetCustomer(formatOutput,dtoOut);
    }


    @Override
    protected InvocadorTransaccion<?, ?> getTransaction() {
        return transaccionPew2;
    }
}
