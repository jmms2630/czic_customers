package com.bbva.czic.customers.dao.impl;

import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.dao.ICustomerDAO;
import com.bbva.czic.customers.dao.tx.TxCreateCustomer;
import com.bbva.czic.customers.dao.tx.TxGetCustomr;
import com.bbva.czic.customers.dao.tx.TxModifyCustomer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Entelgy on 11/08/2016.
 */
@Component(value = "customerDAO")
public class CustomerDao implements ICustomerDAO {

    @Resource(name = "txGetCustomer")
    private TxGetCustomr txGetCustomer;

    @Resource(name ="txCreateCustomer")
    private TxCreateCustomer txCreateCustomer;

    @Resource(name ="txModifyCustomer")
    private TxModifyCustomer txModifyCustomer;


    @Override
    public DTOIntCustomerV2 getCustomer(String customerId) {
        return txGetCustomer.invoke(customerId);
    }

    @Override
    public String createCustomer(DTOIntCustomerV2 dtoIntCustomer) {
        return txCreateCustomer.invoke(dtoIntCustomer);
    }

    @Override
    public void modifyCustomer(DTOIntCustomerV2 dtoIntCustomer) {
        txModifyCustomer.invoke(dtoIntCustomer);
    }
}
