package com.bbva.czic.customers.dao.model.pew4.mock;

import com.bbva.czic.customers.dao.model.pew4.*;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Invocador de la transacci&oacute;n <code>PEW4</code>
 * 
 * @see com.bbva.czic.customers.dao.model.pew4.PeticionTransaccionPew4
 * @see com.bbva.czic.customers.dao.model.pew4.RespuestaTransaccionPew4
 */

@Component(value = "transaccionPew4")
@Profile(value = "dev")
public class TransaccionPew4Mock implements InvocadorTransaccion<PeticionTransaccionPew4,RespuestaTransaccionPew4> {

	@Override
	public RespuestaTransaccionPew4 invocar(final PeticionTransaccionPew4 transaccion) throws ExcepcionTransaccion {

		final RespuestaTransaccionPew4 respuestaTransaccionPew4 = new RespuestaTransaccionPew4();

		final FormatoPEW4CE00 formatoPEW4CE00 = (FormatoPEW4CE00)transaccion.getCuerpo().getPartes().get(0);

		respuestaTransaccionPew4.getCuerpo().getPartes().add(getFormatoPEW4CS00(formatoPEW4CE00.getIdclien()));

		return respuestaTransaccionPew4;
	}

	@Override
	public RespuestaTransaccionPew4 invocarCache(final PeticionTransaccionPew4 transaccion) throws ExcepcionTransaccion {
		return null;
	}

	@Override
	public void vaciarCache() {
	}

	private CopySalida getFormatoPEW4CS00(String id) {

		final CopySalida copy = new CopySalida();


		String ruta = "mocks/FormatoPEW4CS00_Ok.json";
		try {
			final FormatoPEW4CS00 formatoPEW4CS00 = new ObjectMapper().readValue(this.getClass().getClassLoader()
							.getResourceAsStream(ruta), new TypeReference<FormatoPEW4CS00>() {
					});

				copy.setCopy(formatoPEW4CS00);



		} catch (final IOException e) {
			e.printStackTrace();
		}
		return copy;
	}

}