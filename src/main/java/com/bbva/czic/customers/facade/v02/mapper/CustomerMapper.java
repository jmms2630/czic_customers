package com.bbva.czic.customers.facade.v02.mapper;

import com.bbva.czic.customers.business.dto.*;
import com.bbva.czic.customers.facade.v02.dto.*;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;

@Component(value = "customerMapper")
public class CustomerMapper implements ICustomerMapper {

	@Override
	public DataCustomer customerMap(DTOIntCustomerV2 dtoIntCustomer) {

		final DataCustomer dataCustomer = new DataCustomer();
		final Customer customer = new Customer();
		Type type = new Type();
		Country country = new Country();


		customer.setCustomerId(dtoIntCustomer.getCustomerId());
		customer.setFirstName(dtoIntCustomer.getFirstName());
		customer.setLastName(dtoIntCustomer.getLastName());
		customer.setBirthDate(dtoIntCustomer.getBirthDate());
		customer.setMaritalStatus(dtoIntCustomer.getMaritalStatus());
		customer.setGender(dtoIntCustomer.getGender());

		final List<PartnerInformation> partnerInformations =new ArrayList<PartnerInformation>();
		final PartnerInformation partnerInformation = new PartnerInformation();
		type.setId(dtoIntCustomer.getPartnerInformation().getDocumentType().getId());
		type.setName(dtoIntCustomer.getPartnerInformation().getDocumentType().getName());
		partnerInformation.setDocumentType(type);
		partnerInformation.setDocumentNumber(dtoIntCustomer.getPartnerInformation().getDocumentNumber());
		partnerInformation.setName(dtoIntCustomer.getPartnerInformation().getName());
		partnerInformations.add(partnerInformation);
		customer.setPartnerInformation(partnerInformations);

		final List<Nationalities>nationalities = new ArrayList<Nationalities>();
		for (final DTOIntNationalities dtoIntNationality : dtoIntCustomer.getNationalities()) {

			final Nationalities nationality = new Nationalities();
			nationality.setId(dtoIntNationality.getId());
			nationality.setName(dtoIntNationality.getName());

			nationalities.add(nationality);
		}
		customer.setNationalities(nationalities);

		customer.setHasPassesAway(dtoIntCustomer.getHasPassesAway());

		final List<IdentityDocuments> identityDocuments = new ArrayList<IdentityDocuments>();
		final IdentityDocuments intIdentityDocument = new IdentityDocuments();
		type = new Type();
		type.setId(dtoIntCustomer.getIdentityDocument().getDocumentType().getId());
		type.setName(dtoIntCustomer.getIdentityDocument().getDocumentType().getName());
		intIdentityDocument.setDocumentType(type);
		intIdentityDocument.setDocumentNumber(dtoIntCustomer.getIdentityDocument().getDocumentNumber());
		intIdentityDocument.setExpeditionDate(dtoIntCustomer.getIdentityDocument().getExpeditionDate());
		intIdentityDocument.setExpirationDate(dtoIntCustomer.getIdentityDocument().getExpirationDate());
		country.setId(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getId());
		country.setName(dtoIntCustomer.getIdentityDocument().getCountryExpedition().getName());
		intIdentityDocument.setCountryExpedition(country);
		intIdentityDocument.setStateExpedition(dtoIntCustomer.getIdentityDocument().getStateExpedition());
		intIdentityDocument.setCityExpedition(dtoIntCustomer.getIdentityDocument().getCityExpedition());
		identityDocuments.add(intIdentityDocument);
		customer.setIdentityDocuments(identityDocuments);

		customer.setNumberPersonAssociated(dtoIntCustomer.getNumberPersonAssociated());

		final List<LegalAddresses> legalAddresses = new ArrayList<LegalAddresses>();
		final LegalAddresses legalAddress = new LegalAddresses();
		country = new Country();
		country.setId(dtoIntCustomer.getLegalAddresses().getCountry().getId());
		country.setName(dtoIntCustomer.getLegalAddresses().getCountry().getName());
		legalAddress.setCountry(country);
		legalAddress.setType(dtoIntCustomer.getLegalAddresses().getType());
		legalAddress.setRegion(dtoIntCustomer.getLegalAddresses().getRegion());
		legalAddress.setCity(dtoIntCustomer.getLegalAddresses().getCity());
		legalAddress.setAddressName(dtoIntCustomer.getLegalAddresses().getAddressName());
		legalAddress.setZipCode(dtoIntCustomer.getLegalAddresses().getZipCode());
		legalAddresses.add(legalAddress);
		customer.setLegalAddresses(legalAddresses);

		final List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
		for(final DTOIntContactDetail dtoIntContactDetail:dtoIntCustomer.getContactDetail()) {
			final ContactDetail contactDetail = new ContactDetail();
			type = new Type();
			type.setId(dtoIntContactDetail.getContactType().getId());
			type.setName(dtoIntContactDetail.getContactType().getName());
			contactDetail.setContactType(type);
			contactDetail.setId(dtoIntContactDetail.getId());
			contactDetail.setContact(dtoIntContactDetail.getContact());
			contactDetail.setIsChecked(dtoIntContactDetail.getIsChecked());
			contactDetail.setIsPreferential(dtoIntContactDetail.getIsPreferential());
			contactDetails.add(contactDetail);
		}
		customer.setContactDetails(contactDetails);

		final BankingRelationship bankingRelationship =new BankingRelationship();
		bankingRelationship.setCustomerFlag(dtoIntCustomer.getBankingRelationship().getCustomerFlag());
		bankingRelationship.setDebitFlag(dtoIntCustomer.getBankingRelationship().getDebitFlag());
		bankingRelationship.setPremiumFlag(dtoIntCustomer.getBankingRelationship().getPremiumFlag());
		bankingRelationship.setManager(dtoIntCustomer.getBankingRelationship().getManager());
		customer.setBankingRelationship(bankingRelationship);

		final LevelStudy levelStudy = new LevelStudy();
		country = new Country();
		country.setId(dtoIntCustomer.getLevelStudy().getCountry().getId());
		country.setName(dtoIntCustomer.getLevelStudy().getCountry().getName());
		levelStudy.setCountry(country);
		levelStudy.setId(dtoIntCustomer.getLevelStudy().getId());
		levelStudy.setTitle(dtoIntCustomer.getLevelStudy().getTitle());
		levelStudy.setState(dtoIntCustomer.getLevelStudy().getState());
		levelStudy.setCity(dtoIntCustomer.getLevelStudy().getCity());
		customer.setLevelStudy(levelStudy);

		final LivingPlace livingPlace = new LivingPlace();
		type = new Type();
		type.setId(dtoIntCustomer.getLivingPlace().getLivingtype().getId());
		type.setName(dtoIntCustomer.getLivingPlace().getLivingtype().getName());
		livingPlace.setLivingtype(type);
		type = new Type();
		type.setId(dtoIntCustomer.getLivingPlace().getRelationType().getId());
		type.setName(dtoIntCustomer.getLivingPlace().getRelationType().getName());
		livingPlace.setRelationType(type);
		livingPlace.setLivingLevel(dtoIntCustomer.getLivingPlace().getLivingLevel());
		customer.setLivingPlace(livingPlace);

		final EmploymentInformation employmentInformation = new EmploymentInformation();
		final PurpouseCompany purpouseCompany = new PurpouseCompany();
		employmentInformation.setJobTittle(dtoIntCustomer.getEmploymentInformation().getJobTittle());
		employmentInformation.setIsHolder(dtoIntCustomer.getEmploymentInformation().getIsHolder());
		employmentInformation.setTypeEmployee(dtoIntCustomer.getEmploymentInformation().getTypeEmployee());
		employmentInformation.setStartDate(dtoIntCustomer.getEmploymentInformation().getStartDate());
		employmentInformation.setYearsAntiguaty(dtoIntCustomer.getEmploymentInformation().getYearsAntiguaty());
		purpouseCompany.setId(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getId());
		purpouseCompany.setName(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getName());
		purpouseCompany.setCode(dtoIntCustomer.getEmploymentInformation().getPurpouseCompany().getCode());
		employmentInformation.setPurpouseCompany(purpouseCompany);
		customer.setEmploymentInformation(employmentInformation);

		customer.setIsPublicPerson(dtoIntCustomer.getIsPublicPerson());

		dataCustomer.setData(customer);
		return dataCustomer;
		
	}

	public DTOIntCustomerV2 mapInCreateCustomer(final Customer customer) {

		DTOIntCustomerV2 dtoIntCustomer = new DTOIntCustomerV2();
		DTOIntType dtoIntType = new DTOIntType();
		DTOIntCountry dtoIntCountry = new DTOIntCountry();


		dtoIntCustomer.setCustomerId(customer.getCustomerId());
		dtoIntCustomer.setFirstName(customer.getFirstName());
		dtoIntCustomer.setLastName(customer.getLastName());
		dtoIntCustomer.setBirthDate(customer.getBirthDate());
		dtoIntCustomer.setMaritalStatus(customer.getMaritalStatus());
		dtoIntCustomer.setGender(customer.getGender());

		final DTOIntPartnerInformation dtoIntPartnerInformation = new DTOIntPartnerInformation();
		if (customer.getPartnerInformation()!=null && customer.getPartnerInformation().size()>0){
			final PartnerInformation partnerInformation = customer.getPartnerInformation().get(0);
				dtoIntType.setId(partnerInformation.getDocumentType().getId());
				dtoIntType.setName(partnerInformation.getDocumentType().getName());
				dtoIntPartnerInformation.setName(partnerInformation.getName());
				dtoIntPartnerInformation.setDocumentNumber(partnerInformation.getDocumentNumber());
				dtoIntPartnerInformation.setDocumentType(dtoIntType);
		}
		dtoIntCustomer.setPartnerInformation(dtoIntPartnerInformation);

		final List<DTOIntNationalities> dtoIntNationalities = new ArrayList<DTOIntNationalities>();
		DTOIntNationalities dtoIntNationality ;
		if(customer.getNationalities() !=null){
			for(final Nationalities nationalities:customer.getNationalities()) {
				dtoIntNationality = new DTOIntNationalities();
				dtoIntNationality.setId(nationalities.getId());
				dtoIntNationality.setName(nationalities.getName());
				dtoIntNationalities.add(dtoIntNationality);
			}
		}
		dtoIntCustomer.setNationalities(dtoIntNationalities);

		dtoIntCustomer.setHasPassesAway(customer.getHasPassesAway());

		final DTOIntIdentityDocuments dtoIntIdentityDocument = new DTOIntIdentityDocuments();
		if (customer.getIdentityDocuments()!=null && customer.getIdentityDocuments().size()>0){
			final IdentityDocuments identityDocuments = customer.getIdentityDocuments().get(0);
				dtoIntType = new DTOIntType();
				dtoIntType.setId(identityDocuments.getDocumentType().getId());
				dtoIntType.setName(identityDocuments.getDocumentType().getName());
				dtoIntIdentityDocument.setDocumentType(dtoIntType);
				dtoIntIdentityDocument.setDocumentNumber(identityDocuments.getDocumentNumber());
				dtoIntIdentityDocument.setExpeditionDate(identityDocuments.getExpeditionDate());
				dtoIntIdentityDocument.setExpirationDate(identityDocuments.getExpirationDate());
				dtoIntCountry.setId(identityDocuments.getCountryExpedition().getId());
				dtoIntCountry.setName(identityDocuments.getCountryExpedition().getName());
				dtoIntIdentityDocument.setCountryExpedition(dtoIntCountry);
				dtoIntIdentityDocument.setStateExpedition(identityDocuments.getStateExpedition());
				dtoIntIdentityDocument.setCityExpedition(identityDocuments.getCityExpedition());
			}

		dtoIntCustomer.setIdentityDocument(dtoIntIdentityDocument);


		dtoIntCustomer.setNumberPersonAssociated(customer.getNumberPersonAssociated());

		final DTOIntLegalAddresses dtoIntLegalAddress = new DTOIntLegalAddresses();
		if (customer.getLegalAddresses()!=null && customer.getLegalAddresses().size()>0){
			final LegalAddresses legalAddress = customer.getLegalAddresses().get(0);
				dtoIntCountry = new DTOIntCountry();
				dtoIntCountry.setId(legalAddress.getCountry().getId());
				dtoIntCountry.setName(legalAddress.getCountry().getName());
				dtoIntLegalAddress.setCountry(dtoIntCountry);
				dtoIntLegalAddress.setType(legalAddress.getType());
				dtoIntLegalAddress.setRegion(legalAddress.getRegion());
				dtoIntLegalAddress.setCity(legalAddress.getCity());
				dtoIntLegalAddress.setAddressName(legalAddress.getAddressName());
				dtoIntLegalAddress.setZipCode(legalAddress.getZipCode());
			}
		dtoIntCustomer.setLegalAddresses(dtoIntLegalAddress);

		final List<DTOIntContactDetail> dtoIntContactDetails = new ArrayList<DTOIntContactDetail>();
		DTOIntContactDetail dtoIntContactDetail;
		if(customer.getContactDetails()!=null) {
			for (final ContactDetail contactDetail : customer.getContactDetails()) {
				dtoIntContactDetail = new DTOIntContactDetail();
				dtoIntType = new DTOIntType();
				dtoIntType.setId(contactDetail.getContactType().getId());
				dtoIntType.setName(contactDetail.getContactType().getName());
				dtoIntContactDetail.setContactType(dtoIntType);
				dtoIntContactDetail.setId(contactDetail.getId());
				dtoIntContactDetail.setContact(contactDetail.getContact());
				dtoIntContactDetail.setIsChecked(contactDetail.getIsChecked());
				dtoIntContactDetail.setIsPreferential(contactDetail.getIsPreferential());

				dtoIntContactDetails.add(dtoIntContactDetail);
			}
		}
		dtoIntCustomer.setContactDetail(dtoIntContactDetails);

		final DTOIntBankingRelationship dtoIntBankingRelationship =new DTOIntBankingRelationship();
		dtoIntBankingRelationship.setCustomerFlag(customer.getBankingRelationship().getCustomerFlag());
		dtoIntBankingRelationship.setDebitFlag(customer.getBankingRelationship().getDebitFlag());
		dtoIntBankingRelationship.setPremiumFlag(customer.getBankingRelationship().getPremiumFlag());
		dtoIntBankingRelationship.setManager(customer.getBankingRelationship().getManager());
		dtoIntCustomer.setBankingRelationship(dtoIntBankingRelationship);

		final DTOIntLevelStudy dtoIntLevelStudy = new DTOIntLevelStudy();
		dtoIntCountry = new DTOIntCountry();
		dtoIntCountry.setId(customer.getLevelStudy().getCountry().getId());
		dtoIntCountry.setName(customer.getLevelStudy().getCountry().getName());
		dtoIntLevelStudy.setCountry(dtoIntCountry);
		dtoIntLevelStudy.setId(customer.getLevelStudy().getId());
		dtoIntLevelStudy.setTitle(customer.getLevelStudy().getTitle());
		dtoIntLevelStudy.setState(customer.getLevelStudy().getState());
		dtoIntLevelStudy.setCity(customer.getLevelStudy().getCity());
		dtoIntCustomer.setLevelStudy(dtoIntLevelStudy);


		final DTOIntLivingPlace dtoIntLivingPlace = new DTOIntLivingPlace();
		dtoIntType = new DTOIntType();
		dtoIntType.setId(customer.getLivingPlace().getLivingtype().getId());
		dtoIntType.setName(customer.getLivingPlace().getLivingtype().getName());
		dtoIntLivingPlace.setLivingtype(dtoIntType);
		dtoIntType = new DTOIntType();
		dtoIntType.setId(customer.getLivingPlace().getRelationType().getId());
		dtoIntType.setName(customer.getLivingPlace().getRelationType().getName());
		dtoIntLivingPlace.setRelationType(dtoIntType);
		dtoIntLivingPlace.setLivingLevel(customer.getLivingPlace().getLivingLevel());
		dtoIntCustomer.setLivingPlace(dtoIntLivingPlace);

		final DTOIntEmploymentInformation dtoIntEmploymentInformation = new DTOIntEmploymentInformation();
		dtoIntEmploymentInformation.setJobTittle(customer.getEmploymentInformation().getJobTittle());
		dtoIntEmploymentInformation.setIsHolder(customer.getEmploymentInformation().getIsHolder());
		dtoIntEmploymentInformation.setTypeEmployee(customer.getEmploymentInformation().getTypeEmployee());
		dtoIntEmploymentInformation.setStartDate(customer.getEmploymentInformation().getStartDate());
		dtoIntEmploymentInformation.setYearsAntiguaty(customer.getEmploymentInformation().getYearsAntiguaty());
		final DTOIntPurpouseCompany dtoIntPurpouseCompany = new DTOIntPurpouseCompany();
		dtoIntPurpouseCompany.setId(customer.getEmploymentInformation().getPurpouseCompany().getId());
		dtoIntPurpouseCompany.setName(customer.getEmploymentInformation().getPurpouseCompany().getName());
		dtoIntPurpouseCompany.setCode(customer.getEmploymentInformation().getPurpouseCompany().getCode());
		dtoIntEmploymentInformation.setPurpouseCompany(dtoIntPurpouseCompany);
		dtoIntCustomer.setEmploymentInformation(dtoIntEmploymentInformation);

		dtoIntCustomer.setIsPublicPerson(customer.getIsPublicPerson());

		return dtoIntCustomer;
	}



	@Override
	public String mapOutCreateCustomer(final String customerNumber) {
		return customerNumber;
	}

	@Override
	public DTOIntCustomerV2 mapInModifyCustomer(Customer customer) {
		return mapInCreateCustomer(customer);
	}
}

