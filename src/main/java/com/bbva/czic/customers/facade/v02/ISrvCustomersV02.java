package com.bbva.czic.customers.facade.v02;

import com.bbva.czic.customers.facade.v02.dto.Customer;
import com.bbva.czic.customers.facade.v02.dto.DataCustomer;

import javax.ws.rs.core.Response;

public interface ISrvCustomersV02 {

	public String createCustomers(Customer customer);

	public DataCustomer getCustomer(String customerId);

	public Response modifyCustomer(String customerId, Customer customer);

}