package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "identityDocuments", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "identityDocuments", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentityDocuments implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private Type documentType;

	@ApiModelProperty("")
	private String documentNumber;

	@ApiModelProperty("")
	private Date expeditionDate;

	@ApiModelProperty("")
	private Date expirationDate;

	@ApiModelProperty("")
	private Country countryExpedition;

	@ApiModelProperty("")
	private String stateExpedition;

	@ApiModelProperty("")
	private String cityExpedition;

	public IdentityDocuments() {
		// default constructor
	}

	public Type getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Type documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Date getExpeditionDate() {
		return expeditionDate;
	}

	public void setExpeditionDate(Date expeditionDate) {
		this.expeditionDate = expeditionDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Country getCountryExpedition() {
		return countryExpedition;
	}

	public void setCountryExpedition(Country countryExpedition) {
		this.countryExpedition = countryExpedition;
	}

	public String getStateExpedition() {
		return stateExpedition;
	}

	public void setStateExpedition(String stateExpedition) {
		this.stateExpedition = stateExpedition;
	}

	public String getCityExpedition() {
		return cityExpedition;
	}

	public void setCityExpedition(String cityExpedition) {
		this.cityExpedition = cityExpedition;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("documentType", documentType)
				.append("documentNumber", documentNumber).append("expeditionDate", expeditionDate)
				.append("expirationDate", expirationDate).append("countryExpedition", countryExpedition)
				.append("stateExpedition", stateExpedition).append("cityExpedition", cityExpedition).toString();
	}
}
