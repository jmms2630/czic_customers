package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "bankingRelationship", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "bankingRelationship", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankingRelationship implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private String manager;

	@ApiModelProperty("")
	private String customerFlag;

	@ApiModelProperty("")
	private String premiumFlag;

	@ApiModelProperty("")
	private String debitFlag;

	public BankingRelationship() {
		// default constructor
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getCustomerFlag() {
		return customerFlag;
	}

	public void setCustomerFlag(String customerFlag) {
		this.customerFlag = customerFlag;
	}

	public String getPremiumFlag() {
		return premiumFlag;
	}

	public void setPremiumFlag(String premiumFlag) {
		this.premiumFlag = premiumFlag;
	}

	public String getDebitFlag() {
		return debitFlag;
	}

	public void setDebitFlag(String debitFlag) {
		this.debitFlag = debitFlag;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("manager", manager)
				.append("customerFlag", customerFlag).append("premiumFlag", premiumFlag).append("debitFlag", debitFlag)
				.toString();
	}
}
