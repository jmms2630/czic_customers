package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "contactDetail", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "contactDetail", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetail implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private String id;

	@ApiModelProperty("")
	private String contact;

	@ApiModelProperty("")
	private Type contactType;

	@ApiModelProperty("")
	private Boolean isPreferential;

	@ApiModelProperty("")
	private Boolean isChecked;

	public ContactDetail() {
		// default constructor
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Type getContactType() {
		return contactType;
	}

	public void setContactType(Type contactType) {
		this.contactType = contactType;
	}

	public Boolean getIsPreferential() {
		return isPreferential;
	}

	public void setIsPreferential(Boolean isPreferential) {
		this.isPreferential = isPreferential;
	}

	public Boolean getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Boolean isChecked) {
		this.isChecked = isChecked;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("contact", contact)
				.append("contactType", contactType).append("isPreferential", isPreferential)
				.append("isChecked", isChecked).toString();
	}
}
