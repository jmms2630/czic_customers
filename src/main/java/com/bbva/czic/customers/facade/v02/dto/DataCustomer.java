package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "dataCustomer", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "dataCustomer", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataCustomer implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private Customer data;

	public DataCustomer() {
		// default constructor
	}

	public Customer getData() {
		return data;
	}

	public void setData(Customer data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("data", data).toString();
	}
}
