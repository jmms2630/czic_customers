package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@XmlRootElement(name = "customers", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "customers", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer implements Serializable {

	public final static long serialVersionUID = 1L;

	private String customerId;

	private String firstName;

	private String lastName;

	private Date birthDate;

	private String maritalStatus;

	private String gender;

	private List<PartnerInformation> partnerInformation;

	private List<Nationalities> nationalities;

	private String hasPassesAway;

	private List<IdentityDocuments> identityDocuments;

	private String numberPersonAssociated;

	private List<LegalAddresses> legalAddresses;

	private List<ContactDetail> contactDetails;

	private BankingRelationship bankingRelationship;

	private LevelStudy levelStudy;

	private LivingPlace livingPlace;

	private EmploymentInformation employmentInformation;


	private String isPublicPerson;

	public Customer() {
		// default constructor
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<PartnerInformation> getPartnerInformation() {
		return partnerInformation;
	}

	public void setPartnerInformation(List<PartnerInformation> partnerInformation) {
		this.partnerInformation = partnerInformation;
	}

	public List<Nationalities> getNationalities() {
		return nationalities;
	}

	public void setNationalities(List<Nationalities> nationalities) {
		this.nationalities = nationalities;
	}

	public String getHasPassesAway() {
		return hasPassesAway;
	}

	public void setHasPassesAway(String hasPassesAway) {
		this.hasPassesAway = hasPassesAway;
	}

	public List<IdentityDocuments> getIdentityDocuments() {
		return identityDocuments;
	}

	public void setIdentityDocuments(List<IdentityDocuments> identityDocuments) {
		this.identityDocuments = identityDocuments;
	}

	public String getNumberPersonAssociated() {
		return numberPersonAssociated;
	}

	public void setNumberPersonAssociated(String numberPersonAssociated) {
		this.numberPersonAssociated = numberPersonAssociated;
	}

	public List<LegalAddresses> getLegalAddresses() {
		return legalAddresses;
	}

	public void setLegalAddresses(List<LegalAddresses> legalAddresses) {
		this.legalAddresses = legalAddresses;
	}

	public List<ContactDetail> getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(List<ContactDetail> contactDetails) {
		this.contactDetails = contactDetails;
	}

	public BankingRelationship getBankingRelationship() {
		return bankingRelationship;
	}

	public void setBankingRelationship(BankingRelationship bankingRelationship) {
		this.bankingRelationship = bankingRelationship;
	}

	public LevelStudy getLevelStudy() {
		return levelStudy;
	}

	public void setLevelStudy(LevelStudy levelStudy) {
		this.levelStudy = levelStudy;
	}

	public LivingPlace getLivingPlace() {
		return livingPlace;
	}

	public void setLivingPlace(LivingPlace livingPlace) {
		this.livingPlace = livingPlace;
	}

	public EmploymentInformation getEmploymentInformation() {
		return employmentInformation;
	}

	public void setEmploymentInformation(EmploymentInformation employmentInformation) {
		this.employmentInformation = employmentInformation;
	}

	public String getIsPublicPerson() {
		return isPublicPerson;
	}

	public void setIsPublicPerson(String isPublicPerson) {
		this.isPublicPerson = isPublicPerson;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("customerId", customerId)
				.append("firstName", firstName).append("lastName", lastName).append("birthDate", birthDate)
				.append("maritalStatus", maritalStatus).append("gender", gender)
				.append("partnerInformation", (partnerInformation != null) ? partnerInformation.size() : "null")
				.append("nationalities", (nationalities != null) ? nationalities.size() : "null")
				.append("hasPassesAway", hasPassesAway)
				.append("identityDocuments", (identityDocuments != null) ? identityDocuments.size() : "null")
				.append("numberPersonAssociated", numberPersonAssociated)
				.append("legalAddresses", (legalAddresses != null) ? legalAddresses.size() : "null")
				.append("contactDetails", (contactDetails != null) ? contactDetails.size() : "null")
				.append("bankingRelationship", bankingRelationship).append("levelStudy", levelStudy)
				.append("livingPlace", livingPlace).append("employmentInformation", employmentInformation)
				.append("isPublicPerson", isPublicPerson).toString();

	}
}
