package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "livingPlace", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "livingPlace", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class LivingPlace implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private Type livingtype;

	@ApiModelProperty("")
	private Type relationType;

	@ApiModelProperty("")
	private Integer livingLevel;

	public LivingPlace() {
		// default constructor
	}

	public Type getLivingtype() {
		return livingtype;
	}

	public void setLivingtype(Type livingtype) {
		this.livingtype = livingtype;
	}

	public Type getRelationType() {
		return relationType;
	}

	public void setRelationType(Type relationType) {
		this.relationType = relationType;
	}

	public Integer getLivingLevel() {
		return livingLevel;
	}

	public void setLivingLevel(Integer livingLevel) {
		this.livingLevel = livingLevel;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("livingtype", livingtype)
				.append("relationType", relationType).append("livingLevel", livingLevel).toString();
	}
}
