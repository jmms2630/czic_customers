package com.bbva.czic.customers.facade.v02.mapper;

import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.facade.v02.dto.Customer;
import com.bbva.czic.customers.facade.v02.dto.DataCustomer;

/**
 * Created by Entelgy on 9/08/2016.
 */
public interface ICustomerMapper {
    DataCustomer customerMap(DTOIntCustomerV2 dtoIntCustomer);

    DTOIntCustomerV2 mapInCreateCustomer(Customer customer);

    String mapOutCreateCustomer(String customerNumber);

    DTOIntCustomerV2 mapInModifyCustomer(Customer customer);
}
