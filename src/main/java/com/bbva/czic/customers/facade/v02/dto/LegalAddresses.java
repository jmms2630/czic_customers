package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "legalAddresses", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "legalAddresses", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class LegalAddresses implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private String type;

	@ApiModelProperty("")
	private Country country;

	@ApiModelProperty("")
	private String region;

	@ApiModelProperty("")
	private String city;

	@ApiModelProperty("")
	private String addressName;

	@ApiModelProperty("")
	private String zipCode;

	public LegalAddresses() {
		// default constructor
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddressName() {
		return addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("type", type)
				.append("country", country).append("region", region).append("city", city)
				.append("addressName", addressName).append("zipCode", zipCode).toString();
	}
}
