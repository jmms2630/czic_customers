package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "employmentInformation", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "employmentInformation", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmploymentInformation implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private String jobTittle;

	@ApiModelProperty("")
	private String isHolder;

	@ApiModelProperty("")
	private String typeEmployee;

	@ApiModelProperty("")
	private Date startDate;

	@ApiModelProperty("")
	private Integer yearsAntiguaty;

	@ApiModelProperty("")
	private PurpouseCompany purpouseCompany;

	public EmploymentInformation() {
		// default constructor
	}

	public String getJobTittle() {
		return jobTittle;
	}

	public void setJobTittle(String jobTittle) {
		this.jobTittle = jobTittle;
	}

	public String getIsHolder() {
		return isHolder;
	}

	public void setIsHolder(String isHolder) {
		this.isHolder = isHolder;
	}

	public String getTypeEmployee() {
		return typeEmployee;
	}

	public void setTypeEmployee(String typeEmployee) {
		this.typeEmployee = typeEmployee;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getYearsAntiguaty() {
		return yearsAntiguaty;
	}

	public void setYearsAntiguaty(Integer yearsAntiguaty) {
		this.yearsAntiguaty = yearsAntiguaty;
	}

	public PurpouseCompany getPurpouseCompany() {
		return purpouseCompany;
	}

	public void setPurpouseCompany(PurpouseCompany purpouseCompany) {
		this.purpouseCompany = purpouseCompany;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("jobTittle", jobTittle)
				.append("isHolder", isHolder).append("typeEmployee", typeEmployee).append("startDate", startDate)
				.append("yearsAntiguaty", yearsAntiguaty).append("purpouseCompany", purpouseCompany).toString();
	}
}
