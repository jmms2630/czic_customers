package com.bbva.czic.customers.facade.v02.impl;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.bbva.czic.customers.business.ISrvIntCustomer;
import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.facade.v02.dto.Customer;
import com.bbva.czic.customers.facade.v02.mapper.ICustomerMapper;
import com.bbva.zic.commons.rm.utils.CallSequenceMessageEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.czic.customers.facade.v02.ISrvCustomersV02;
import com.bbva.czic.customers.facade.v02.dto.DataCustomer;
import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import static com.bbva.zic.commons.rm.utils.LogUtils.buildLogMessage;


@Path("/V02")
@SN(registryID = "SN201400333", logicalID = "customers")
@VN(vnn = "V02")
@Api(value = "/customersnet/V02", description = "SN customers")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
@Service
public class SrvCustomersV02 implements ISrvCustomersV02, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static Log log = LogFactory.getLog(SrvCustomersV02.class);

	public HttpHeaders httpHeaders;

	@Autowired
	BusinessServicesToolKit bussinesToolKit;

	public UriInfo uriInfo;

	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	//@Autowired
	//ISrvIntCustomers srvIntCustomers;

	@Resource(name = "srvIntCustomer")
	private ISrvIntCustomer srvIntCustomer;

	@Resource(name = "customerMapper")
	private ICustomerMapper mapper;

	@Override
	@ApiOperation(value = "Service for creating a customer.", notes = "Service for creating a customer.")
	@ApiResponses(value = { @ApiResponse(code = -1, message = "aliasGCE1"),
			@ApiResponse(code = -1, message = "aliasGCE2"),
			@ApiResponse(code = 200, message = "Found Sucessfully"),
			@ApiResponse(code = 500, message = "Technical Error") })
	@POST
	@SMC(registryID = "SMC201400334", logicalID = "createCustomers")
	public String createCustomers(@ApiParam(value = "customer")
	final Customer customer) {
		log.debug(buildLogMessage(CallSequenceMessageEnum.FACADE_REQUEST, customer));

		final DTOIntCustomerV2 dtoIntCustomerV2 = mapper.mapInCreateCustomer(customer);

		final String  customerNumber = mapper.mapOutCreateCustomer(srvIntCustomer.createCustomer(dtoIntCustomerV2));

		log.debug(buildLogMessage(CallSequenceMessageEnum.FACADE_RESPONSE, customerNumber));

		return customerNumber;
	}

	@Override
	@ApiOperation(value = "Service for retrieving the customer's information.", notes = "Service for retrieving the customer's information.", response = DataCustomer.class)
	@ApiResponses(value = { @ApiResponse(code = -1, message = "aliasGCE1"),
			@ApiResponse(code = -1, message = "aliasGCE2"),
			@ApiResponse(code = 200, message = "Found Sucessfully", response = DataCustomer.class),
			@ApiResponse(code = 500, message = "Technical Error") })
	@GET
	@Path("/{customerId}")
	@SMC(registryID = "SMC201400334", logicalID = "getCustomer")
	public DataCustomer getCustomer(@ApiParam(value = "customerId")
	@PathParam("customerId")
	final String customerId) {
		log.info(buildLogMessage(CallSequenceMessageEnum.FACADE_REQUEST, customerId));

		final DataCustomer outGetCustomer = mapper.customerMap(srvIntCustomer.getCustomer(customerId));

		log.info(buildLogMessage(CallSequenceMessageEnum.FACADE_RESPONSE, outGetCustomer));

		return outGetCustomer;
	}

	@Override
	@ApiOperation(value = "Service for updating the customer's information.", notes = "Service for updating the customer's information.", response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = -1, message = "aliasGCE1"),
			@ApiResponse(code = -1, message = "aliasGCE2"),
			@ApiResponse(code = 200, message = "Found Sucessfully", response = Response.class),
			@ApiResponse(code = 500, message = "Technical Error") })
	@PATCH
	@Path("/{customerId}")
	@SMC(registryID = "SMC201400334", logicalID = "modifyCustomer")
	public Response modifyCustomer(@ApiParam(value = "customerId")
	@PathParam("customerId")
	final String customerId, @ApiParam(value = "customer")
	final Customer customer) {
		log.info(buildLogMessage(CallSequenceMessageEnum.FACADE_REQUEST,customer));
		final DTOIntCustomerV2 dtoIntCustomer = mapper.mapInModifyCustomer(customer);
		srvIntCustomer.modifyCustomer(dtoIntCustomer);
		return Response.noContent().build();
	}

}
