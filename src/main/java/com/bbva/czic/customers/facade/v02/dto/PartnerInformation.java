package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "partnerInformation", namespace = "urn:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "partnerInformation", namespace = "bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PartnerInformation implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private Type documentType;

	@ApiModelProperty("")
	private String documentNumber;

	@ApiModelProperty("")
	private String name;

	public PartnerInformation() {
		// default constructor
	}

	public Type getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Type documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("documentType", documentType)
				.append("documentNumber", documentNumber).append("name", name).toString();
	}
}
