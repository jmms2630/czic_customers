package com.bbva.czic.customers.facade.v02.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "levelStudy", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlType(name = "levelStudy", namespace = "urn:com:bbva:czic:customers:facade:v02:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class LevelStudy implements Serializable {

	public final static long serialVersionUID = 1L;

	@ApiModelProperty("")
	private String id;

	@ApiModelProperty("")
	private String title;

	@ApiModelProperty("")
	private Country country;

	@ApiModelProperty("")
	private String state;

	@ApiModelProperty("")
	private String city;

	public LevelStudy() {
		// default constructor
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("title", title)
				.append("country", country).append("state", state).append("city", city).toString();
	}
}
