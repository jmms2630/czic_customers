package com.bbva.czic.customers.business.dto;

import java.util.Date;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntIdentityDocuments {

    public final static long serialVersionUID = 1L;

    private DTOIntType documentType;

    private String documentNumber;

    private Date expeditionDate;

    private Date expirationDate;

    private DTOIntCountry countryExpedition;

    private String stateExpedition;

    private String cityExpedition;

    public DTOIntIdentityDocuments() {
        // default constructor
    }

    public DTOIntType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getExpeditionDate() {
        return expeditionDate;
    }

    public void setExpeditionDate(Date expeditionDate) {
        this.expeditionDate = expeditionDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public DTOIntCountry getCountryExpedition() {
        return countryExpedition;
    }

    public void setCountryExpedition(DTOIntCountry countryExpedition) {
        this.countryExpedition = countryExpedition;
    }

    public String getStateExpedition() {
        return stateExpedition;
    }

    public void setStateExpedition(String stateExpedition) {
        this.stateExpedition = stateExpedition;
    }

    public String getCityExpedition() {
        return cityExpedition;
    }

    public void setCityExpedition(String cityExpedition) {
        this.cityExpedition = cityExpedition;
    }
}
