package com.bbva.czic.customers.business;

import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;

/**
 * Created by Entelgy on 11/08/2016.
 */
public interface ISrvIntCustomer {

    DTOIntCustomerV2 getCustomer(String customerId);


    String createCustomer(DTOIntCustomerV2 dtoIntCustomerV2);

    void modifyCustomer(DTOIntCustomerV2 dtoIntCustomer);
}
