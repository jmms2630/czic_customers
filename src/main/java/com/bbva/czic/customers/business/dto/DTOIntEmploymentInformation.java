package com.bbva.czic.customers.business.dto;

import java.util.Date;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntEmploymentInformation {

    public final static long serialVersionUID = 1L;

    private String jobTittle;

    private String isHolder;

    private String typeEmployee;

    private Date startDate;

    private Integer yearsAntiguaty;

    private DTOIntPurpouseCompany purpouseCompany;

    public DTOIntEmploymentInformation() {
        // default constructor
    }

    public String getJobTittle() {
        return jobTittle;
    }

    public void setJobTittle(String jobTittle) {
        this.jobTittle = jobTittle;
    }

    public String getIsHolder() {
        return isHolder;
    }

    public void setIsHolder(String isHolder) {
        this.isHolder = isHolder;
    }

    public String getTypeEmployee() {
        return typeEmployee;
    }

    public void setTypeEmployee(String typeEmployee) {
        this.typeEmployee = typeEmployee;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getYearsAntiguaty() {
        return yearsAntiguaty;
    }

    public void setYearsAntiguaty(Integer yearsAntiguaty) {
        this.yearsAntiguaty = yearsAntiguaty;
    }

    public DTOIntPurpouseCompany getPurpouseCompany() {
        return purpouseCompany;
    }

    public void setPurpouseCompany(DTOIntPurpouseCompany purpouseCompany) {
        this.purpouseCompany = purpouseCompany;
    }
}
