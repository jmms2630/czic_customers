package com.bbva.czic.customers.business.dto;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntLegalAddresses {

    public final static long serialVersionUID = 1L;

    private String type;

    private DTOIntCountry country;

    private String region;

    private String city;

    private String addressName;

    private String zipCode;

    public DTOIntLegalAddresses() {
        // default constructor
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DTOIntCountry getCountry() {
        return country;
    }

    public void setCountry(DTOIntCountry country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
