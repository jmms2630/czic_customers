package com.bbva.czic.customers.business.dto;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntContactDetail {

    public final static long serialVersionUID = 1L;

    private String id;

    private String contact;

    private DTOIntType contactType;

    private Boolean isPreferential;

    private Boolean isChecked;

    public DTOIntContactDetail() {
        // default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public DTOIntType getContactType() {
        return contactType;
    }

    public void setContactType(DTOIntType contactType) {
        this.contactType = contactType;
    }

    public Boolean getIsPreferential() {
        return isPreferential;
    }

    public void setIsPreferential(Boolean isPreferential) {
        this.isPreferential = isPreferential;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }
}
