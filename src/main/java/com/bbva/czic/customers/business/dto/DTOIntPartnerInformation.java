package com.bbva.czic.customers.business.dto;

/**
 * Created by Entelgy on 10/08/2016.
 */
public class DTOIntPartnerInformation {

    public final static long serialVersionUID = 1L;
    private DTOIntType documentType;
    private String documentNumber;
    private String name;

    public DTOIntPartnerInformation() {
        // default constructor
    }

    public DTOIntType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
