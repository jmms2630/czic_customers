package com.bbva.czic.customers.business.dto;


/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntBankingRelationship {

    public final static long serialVersionUID = 1L;

    private String manager;

    private String customerFlag;

    private String premiumFlag;

    private String debitFlag;

    public DTOIntBankingRelationship() {
        // default constructor
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getCustomerFlag() {
        return customerFlag;
    }

    public void setCustomerFlag(String customerFlag) {
        this.customerFlag = customerFlag;
    }

    public String getPremiumFlag() {
        return premiumFlag;
    }

    public void setPremiumFlag(String premiumFlag) {
        this.premiumFlag = premiumFlag;
    }

    public String getDebitFlag() {
        return debitFlag;
    }

    public void setDebitFlag(String debitFlag) {
        this.debitFlag = debitFlag;
    }
}
