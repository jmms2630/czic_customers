package com.bbva.czic.customers.business.impl;

import com.bbva.czic.customers.business.ISrvIntCustomer;
import com.bbva.czic.customers.business.dto.DTOIntCustomerV2;
import com.bbva.czic.customers.dao.ICustomerDAO;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.zic.commons.rm.utils.CallSequenceMessageEnum;
import com.bbva.zic.commons.rm.utils.validations.impl.StringValidator;
import com.bbva.zic.commons.rm.utils.validator.DtoValidator;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.bbva.zic.commons.rm.utils.LogUtils.buildLogMessage;

/**
 * Created by Entelgy on 11/08/2016.
 */
@Service(value = "srvIntCustomer")
public class SrvIntCustomer implements ISrvIntCustomer {

    private static final String FILTERERROR = null;

    private static I18nLog log = I18nLogFactory.getLogI18n(
            SrvIntCustomer.class, "META-INF/spring/i18n/log/mensajesLog");

    @Resource(name = "customerDAO")
    private ICustomerDAO customerDAO;

    /**
     *
     * @param customerId
     * @return
     * @throws com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException
     */
    @Override
    public DTOIntCustomerV2 getCustomer(final String customerId) {

        log.info(buildLogMessage(CallSequenceMessageEnum.SERVICE_REQUEST, customerId));

        new StringValidator().isNullText(customerId).validate();

        final DTOIntCustomerV2 dtoIntCustomerV2 = customerDAO.getCustomer(customerId);
        dtoIntCustomerV2.setCustomerId(customerId);

        DtoValidator.validate(dtoIntCustomerV2);

        log.info(buildLogMessage(CallSequenceMessageEnum.SERVICE_REQUEST, dtoIntCustomerV2));

        return dtoIntCustomerV2;
    }


    @Override
    public String createCustomer(DTOIntCustomerV2 dtoIntCustomerV2) {

        log.debug(buildLogMessage(CallSequenceMessageEnum.SERVICE_REQUEST, dtoIntCustomerV2));

        DtoValidator.validate(dtoIntCustomerV2);

        final String customerNumber = customerDAO.createCustomer(dtoIntCustomerV2);

        DtoValidator.validate(customerNumber);

        log.debug(buildLogMessage(CallSequenceMessageEnum.SERVICE_REQUEST, customerNumber));

        return customerNumber;
    }

    @Override
    public void modifyCustomer(DTOIntCustomerV2 dtoIntCustomer) {

        log.debug(buildLogMessage(CallSequenceMessageEnum.SERVICE_REQUEST,dtoIntCustomer));

        DtoValidator.validate(dtoIntCustomer);

        customerDAO.modifyCustomer(dtoIntCustomer);

    }
}
