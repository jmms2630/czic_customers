package com.bbva.czic.customers.business.dto;

import java.util.Date;
import java.util.List;

/**
 * Created by Entelgy on 8/08/2016.
 */
public class DTOIntCustomerV2 {

    public final static long serialVersionUID = 1L;

    private String customerId;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String maritalStatus;
    private String gender;
    private DTOIntPartnerInformation partnerInformation;
    private List<DTOIntNationalities> nationalities;
    private String hasPassesAway;
    private DTOIntIdentityDocuments identityDocument;

    private String numberPersonAssociated;
    private DTOIntLegalAddresses legalAddresses;
    private List<DTOIntContactDetail> contactDetail;
    private DTOIntBankingRelationship bankingRelationship;
    private DTOIntLevelStudy levelStudy;
    private DTOIntLivingPlace livingPlace;
    private DTOIntEmploymentInformation employmentInformation;
    private String isPublicPerson;


    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public DTOIntPartnerInformation getPartnerInformation() {
        return partnerInformation;
    }

    public void setPartnerInformation(DTOIntPartnerInformation partnerInformation) {
        this.partnerInformation = partnerInformation;
    }

    public List<DTOIntNationalities> getNationalities() {
        return nationalities;
    }

    public void setNationalities(List<DTOIntNationalities> nationalities) {
        this.nationalities = nationalities;
    }

    public String getHasPassesAway() {
        return hasPassesAway;
    }

    public void setHasPassesAway(String hasPassesAway) {
        this.hasPassesAway = hasPassesAway;
    }

    public DTOIntIdentityDocuments getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocuments identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getNumberPersonAssociated() {
        return numberPersonAssociated;
    }

    public void setNumberPersonAssociated(String numberPersonAssociated) {
        this.numberPersonAssociated = numberPersonAssociated;
    }

    public DTOIntLegalAddresses getLegalAddresses() {
        return legalAddresses;
    }

    public void setLegalAddresses(DTOIntLegalAddresses legalAddresses) {
        this.legalAddresses = legalAddresses;
    }

    public List<DTOIntContactDetail> getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(List<DTOIntContactDetail> contactDetail) {
        this.contactDetail = contactDetail;
    }

    public DTOIntBankingRelationship getBankingRelationship() {
        return bankingRelationship;
    }

    public void setBankingRelationship(DTOIntBankingRelationship bankingRelationship) {
        this.bankingRelationship = bankingRelationship;
    }

    public DTOIntLevelStudy getLevelStudy() {
        return levelStudy;
    }

    public void setLevelStudy(DTOIntLevelStudy levelStudy) {
        this.levelStudy = levelStudy;
    }

    public DTOIntLivingPlace getLivingPlace() {
        return livingPlace;
    }

    public void setLivingPlace(DTOIntLivingPlace livingPlace) {
        this.livingPlace = livingPlace;
    }

    public DTOIntEmploymentInformation getEmploymentInformation() {
        return employmentInformation;
    }

    public void setEmploymentInformation(DTOIntEmploymentInformation employmentInformation) {
        this.employmentInformation = employmentInformation;
    }

    public String getIsPublicPerson() {
        return isPublicPerson;
    }

    public void setIsPublicPerson(String isPublicPerson) {
        this.isPublicPerson = isPublicPerson;
    }

}