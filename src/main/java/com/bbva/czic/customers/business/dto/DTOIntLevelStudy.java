package com.bbva.czic.customers.business.dto;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntLevelStudy {

    public final static long serialVersionUID = 1L;

    private String id;

    private String title;

    private DTOIntCountry country;

    private String state;

    private String city;

    public DTOIntLevelStudy() {
        // default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DTOIntCountry getCountry() {
        return country;
    }

    public void setCountry(DTOIntCountry country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
