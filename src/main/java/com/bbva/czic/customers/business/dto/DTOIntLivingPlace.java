package com.bbva.czic.customers.business.dto;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntLivingPlace {

    public final static long serialVersionUID = 1L;

    private DTOIntType livingtype;

    private DTOIntType relationType;

    private Integer livingLevel;

    public DTOIntLivingPlace() {
        // default constructor
    }

    public DTOIntType getLivingtype() {
        return livingtype;
    }

    public void setLivingtype(DTOIntType livingtype) {
        this.livingtype = livingtype;
    }

    public DTOIntType getRelationType() {
        return relationType;
    }

    public void setRelationType(DTOIntType relationType) {
        this.relationType = relationType;
    }

    public Integer getLivingLevel() {
        return livingLevel;
    }

    public void setLivingLevel(Integer livingLevel) {
        this.livingLevel = livingLevel;
    }
}
