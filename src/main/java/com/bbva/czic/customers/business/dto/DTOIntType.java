package com.bbva.czic.customers.business.dto;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOIntType {

    public final static long serialVersionUID = 1L;

    private String id;

    private String name;

    public DTOIntType() {
        // default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
